# Gépírás gyakorló program

>Gépírás tanító/gyakorló program modellezése SSADM-mel. A megvalósítás történjen
>webes felületen, vagy stand-alone alkalmazásként, két személyes csapat számára. Példa:
>KeyHero

## Követelmények
1. Idézetek, és gyakorló feladatok kezelése az adatbázisban
2. Új felhasználók, idézetek és gyakorló feladato felvétele módosítása az adatbázisban
3. Gyakorló feladatok kategorizálása (pl.: idézetek/gyakorlatok) -nem triviáli
4. Felhasználók írási sebességének nyomon követése az adatbázisban
5. Felhasználók által publikált idézetek elfogadása, állapotuk nyomon követése (pl.: értékelésre vár,
elfogadott, elutasított)
6. Friss idézetek böngészése kategóriák szerint - nem triviális
7. Felhasználók lapján statisztikájuk
8. Adminisztrátori felülelten, elbírálásra váró idézetek listázása, beküldési idő szerint sorrendbe
rakva.
9. Legnépszerübb gyakorlatok listázása, idézetek és gyakorló feladatok szerint kategorizálva - nem
triviális
10. Idézetek, gyakorló feladatok értékelése (like/dislike) -trigger (ha egy idézet vagy gyakorló feladat
arányában túl sok negatív értékelést kap, akkor kitörlődik)
11. Gépelési felület megvalósítása
12. Adatok kiexportálása kategóriák, és idő szerint (json/csv/...) -nem triviális
13. Kitüntetések hozzárendelése felhasználókhoz (pl.: ha valaki 100 wpm fölött gépelt, villámgyors
kitüntetést kap stb...) - trigger