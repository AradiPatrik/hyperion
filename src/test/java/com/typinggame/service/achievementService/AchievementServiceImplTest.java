package com.typinggame.service.achievementService;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.typinggame.di.OfflineModule;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AchievementServiceImplTest {
    private AchievementService achievementService;

    public AchievementServiceImplTest() {
        Injector injector = Guice.createInjector(new OfflineModule());
        achievementService = injector.getInstance(AchievementService.class);
    }

    @Test
    public void getAllAchievementsShouldReturnListWithElements() {
        assertTrue(achievementService.getAllAchievements().size() > 0);
    }

    @Test
    public void getCurrentUserAchievementsShouldReturnEmptyWhenThereIsNoLoggedInUser() {
        assertFalse(achievementService.getCurrentUsersAchievements().isPresent());
    }
}