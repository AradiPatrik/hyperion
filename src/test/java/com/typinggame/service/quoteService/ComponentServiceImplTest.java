package com.typinggame.service.quoteService;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.typinggame.controller.QuoteItemController;
import com.typinggame.di.OfflineModule;
import com.typinggame.service.component.ComponentService;
import com.typinggame.service.component.ComponentServiceImpl;
import com.typinggame.service.component.QuoteComponent;
import javafx.embed.swing.JFXPanel;
import javafx.scene.control.DialogPane;
import javafx.scene.layout.VBox;
import org.junit.Test;

import static org.junit.Assert.*;

public class ComponentServiceImplTest {
    @Test
    public void testGetComponent() {
        Injector injector = Guice.createInjector(new OfflineModule());
        new JFXPanel();
        ComponentService quoteService = injector.getInstance(ComponentService.class);
        QuoteComponent component = quoteService.getQuoteComponent();
        assertEquals(component.controller.getClass(), QuoteItemController.class);
        assertEquals(component.view.getClass(), VBox.class);
    }
}
