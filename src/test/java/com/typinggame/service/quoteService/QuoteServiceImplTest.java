package com.typinggame.service.quoteService;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.typinggame.di.OfflineModule;
import com.typinggame.model.bean.Category;
import com.typinggame.model.bean.Quote;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class QuoteServiceImplTest {
    private QuoteService quoteService;
    private Quote testQuote;

    @Before
    public void setUp() throws Exception {
        Injector injector = Guice.createInjector(new OfflineModule());
        quoteService = injector.getInstance(QuoteService.class);
        testQuote = new Quote();
        testQuote.setId(Integer.MAX_VALUE);
        testQuote.setTitle("test");
    }

    @Test
    public void getGameQuoteShouldReturnTestQuoteAfterSettingIt() {
        quoteService.setGameQuote(testQuote);
        assertEquals(quoteService.getGameQuote().get().getTitle(), testQuote.getTitle());
    }

    @Test
    public void getQuotesShouldReturnAListOfQuotes() {
        assertTrue(quoteService.getQuotes().size() != 0);
    }

    @Test
    public void getQuoteShouldReturnAQuoteAfterSetGameQuoteToRandom() {
        quoteService.setGameQuoteToRandom();
        assertTrue(quoteService.getGameQuote().isPresent());
    }

    @Test
    public void getQuotesOfCategoryShouldReturnListWithElements() {
        assertTrue(quoteService.getQuotesOfCategory(new Category("Poem")).size() > 0);
    }

    @Test
    public void getPendingQuotesShouldReturnWithElements() {
        assertTrue(quoteService.getPendingQuotes().size() > 0);
    }
}