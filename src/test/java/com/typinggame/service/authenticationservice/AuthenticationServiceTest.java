package com.typinggame.service.authenticationservice;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.typinggame.di.OfflineModule;
import com.typinggame.model.bean.User;
import com.typinggame.service.authenticationservice.userexception.IncompleteUserException;
import com.typinggame.service.authenticationservice.userexception.NoneUniqueUsernameException;
import com.typinggame.service.authenticationservice.userexception.UserNotFoundException;
import com.typinggame.service.authenticationservice.userexception.WrongPasswordException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AuthenticationServiceTest {
    AuthenticationService authenticationService;

    @Before
    public void setUp() throws Exception {
        Injector injector = Guice.createInjector(new OfflineModule());
        authenticationService = injector.getInstance(AuthenticationService.class);
    }

    @Test
    public void getUserShouldReturnCorrectUserAfterLogin() {
        try {
            authenticationService.login("test", "test");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertTrue(authenticationService.getLoggedInUser().get().getPassword().equals("test"));
    }

    @Test
    public void getUserShouldNotReturnEmptyAfterLogin() {
        try {
            authenticationService.login("test", "test");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertTrue(authenticationService.getLoggedInUser().isPresent());
    }

    @Test(expected = UserNotFoundException.class)
    public void loginShouldThrowExceptionIfThereIsNoUserWithParameterUsername() throws Exception {
        authenticationService.login("", "");
    }

    @Test(expected = WrongPasswordException.class)
    public void loginShouldThrowExceptionIfThePasswordNotMatchWithParameter() throws Exception {
        authenticationService.login("test", "");
    }

    @Test
    public void signUpShouldNotThrowExceptionWhenValidUserGivenAParameter() throws Exception {
        User newUser = new User();
        newUser.setUsername("ABCDEFGH");
        newUser.setPassword("ABCDEFGH");
        authenticationService.signUp(newUser);
    }

    @Test
    public void getUserShouldReturnNewUserAfterSignup() throws Exception {
        User newUser = new User();
        newUser.setUsername("ABCDEFGH");
        newUser.setPassword("ABCDEFGH");
        authenticationService.signUp(newUser);
        assertTrue(authenticationService.getLoggedInUser().get().getUsername().equals("ABCDEFGH"));
    }

    @Test(expected = NoneUniqueUsernameException.class)
    public void signUpShouldThrowExceptionUserNameIsTaken() throws Exception {
        User newUser = new User();
        newUser.setUsername("ABCDEFGH");
        newUser.setPassword("ABCDEFGH");
        authenticationService.signUp(newUser);
        authenticationService.signUp(newUser);
    }

    @Test(expected = IncompleteUserException.class)
    public void signUpShouldThrowExceptionIfUsernameIsNull() throws Exception {
        User newUser = new User();
        newUser.setPassword("ABCDEFGH");
        authenticationService.signUp(newUser);
    }

    @Test(expected = IncompleteUserException.class)
    public void signUpShouldThrowExceptionIfPassWordIsEmpty() throws Exception {
        User newUser = new User();
        newUser.setUsername("ABCDEFGH");
        newUser.setPassword("");
        authenticationService.signUp(newUser);
    }

    @Test
    public void getUserShouldReturnEmptyOptionalAfterLogout() {
        authenticationService.logout();
        assertFalse(authenticationService.getLoggedInUser().isPresent());
    }
}