package com.typinggame.model.dao.userdao;

import com.typinggame.model.bean.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MemUserDAOImplTest {
    MemUserDAOImpl memUserDAO;

    @Before
    public void setUp() throws Exception {
        memUserDAO = new MemUserDAOImpl();
    }

    @Test
    public void getUserShouldNotReturnNullWithUserIfItIsInDAO() {
        assertNotNull(memUserDAO.getUser("test").get());
    }

    @Test
    public void getUserShouldReturnUserWithCorrectInstanceVariables() {
        assertEquals(memUserDAO.getUser("test").get().getPassword(), "test");
    }

    @Test
    public void addUserShouldAddUserToDAO() {
        getUserShouldNotReturnNullWithUserIfItIsInDAO();
        memUserDAO.addUser(new User("testName", "testPassword", null, null));
        assertNotNull(memUserDAO.getUser("testName").get());
    }

    @Test
    public void getUserShouldReturnEmptyAfterDeleteOfThatUser() {
        User user = new User();
        user.setUsername("test");
        user.setPassword("test");
        memUserDAO.deleteUser(user);
        assertFalse(memUserDAO.getUser("test").isPresent());
    }

}