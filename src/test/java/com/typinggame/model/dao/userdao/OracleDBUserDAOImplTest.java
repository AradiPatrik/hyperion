package com.typinggame.model.dao.userdao;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.typinggame.di.OnlineModule;
import com.typinggame.model.bean.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.*;

public class OracleDBUserDAOImplTest {
    private UserDAO userDAO;
    private User testUser;

    @Before
    public void setUp() throws Exception {
        testUser = new User();
        testUser.setUsername("test");
        testUser.setPassword("test");
        testUser.setRegisterDate(Calendar.getInstance().getTime());
        Injector injector = Guice.createInjector(new OnlineModule());
        userDAO = injector.getInstance(UserDAO.class);
    }

    @After
    public void tearDown() throws Exception {
        userDAO.deleteUser(testUser);
    }

    @Test
    public void getUserShouldReturnNullForEmptyString() {
        assertFalse(userDAO.getUser("").isPresent());
    }

    @Test
    public void getUserShouldReturnEmptyAfterAddAndDeleteOfThatUser() {
        userDAO.addUser(testUser);
        userDAO.deleteUser(testUser);
        assertFalse(userDAO.getUser(testUser.getUsername()).isPresent());
    }

    @Test
    public void getUserShouldReturnUserAfterAddOfThatUser() {
        userDAO.addUser(testUser);
        assertTrue(userDAO.getUser(testUser.getUsername()).get().getUsername().equals(testUser.getUsername()));
    }

    @Test
    public void getUserShouldReturnUser() {
        assertNotNull(userDAO.getUser("Tomi").get());
    }
}