package com.typinggame.model.dao.gamesession;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.typinggame.di.OnlineModule;
import com.typinggame.model.bean.GameSession;
import com.typinggame.model.bean.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.core.Is.*;
import static org.junit.Assert.*;

public class OracleDBGameSessionDAOImplTest {
    private GameSessionDAO gameSessionDAO;

    public OracleDBGameSessionDAOImplTest() {
        Injector injector = Guice.createInjector(new OnlineModule());
        gameSessionDAO = injector.getInstance(GameSessionDAO.class);
    }

    @Test
    public void getGameSessionsOfUser() {
        assertThat(gameSessionDAO.getGameSessionOfUsername("Tomi").size(), is(2));
        assertThat(gameSessionDAO.getGameSessionOfUsername("fd"), is(asList(new GameSession[] {})));
    }

    @Test
    public void addGameSession() {
        int oldNumOfSessionsOfLea = gameSessionDAO.getGameSessionOfUsername("Lea").size();
        gameSessionDAO.addGameSession(new GameSession(8, 40, null, 3, "Lea"));
        assertThat(gameSessionDAO.getGameSessionOfUsername("Lea").size(), is(oldNumOfSessionsOfLea + 1));
        gameSessionDAO.addGameSession(new GameSession(440, 80, null, 3, "Lea"));
        assertThat(gameSessionDAO.getGameSessionOfUsername("Lea").size(), is(oldNumOfSessionsOfLea + 1));
    }
}