package com.typinggame.model.dao.admindao;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.typinggame.di.OnlineModule;
import org.junit.Test;

import static org.junit.Assert.*;

public class OracleAdminDAOImplTest {

    @Test
    public void isUserAdmin() {
        Injector injector = Guice.createInjector(new OnlineModule());
        AdminDAO adminDAO = injector.getInstance(AdminDAO.class);
        assertTrue(adminDAO.authenticateAdmin("AdminPista", "pista123"));
    }
}