package com.typinggame.model.dao.achievementdao;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.typinggame.di.OfflineModule;
import com.typinggame.model.bean.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class MemAchievementDAOImplTest {
    private User testUser;
    private AchievementDAO achievementDAO;

    @Before
    public void setUp() throws Exception {
        testUser = new User();
        testUser.setUsername("Peti");
        testUser.setPassword("peti123");
        Injector injector = Guice.createInjector(new OfflineModule());
        achievementDAO = injector.getInstance(AchievementDAO.class);
    }

    @Test
    public void getAllAchievementShouldReturnAListWithElements() {
        assertTrue(achievementDAO.getAllAchievement().size() > 0);
    }

    @Test
    public void getAchievementsOfUserShouldReturnAListWithElementsForValidUser() {
        assertTrue(achievementDAO.getAchievementsOfUser(testUser).size() > 0);
    }

    @Test
    public void getAchievementsOfUserShouldReturnAListWithSizeZeroForInvalidUser() {
        User invalidUser = new User();
        invalidUser.setUsername("");
        assertTrue(achievementDAO.getAchievementsOfUser(invalidUser).size() == 0);
    }


}