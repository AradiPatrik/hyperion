package com.typinggame.model.dao.quotedao;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.typinggame.di.OfflineModule;
import com.typinggame.model.bean.Category;
import com.typinggame.model.bean.Quote;
import com.typinggame.model.bean.QuoteStatus;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class MemQuoteDAOImplTest {
    private QuoteDAO quoteDAO;
    private Quote testQuote;
    private Category testCategory;

    @Before
    public void setUp() throws Exception {
        Injector injector = Guice.createInjector(new OfflineModule());
        this.quoteDAO = injector.getInstance(QuoteDAO.class);
        testQuote = new Quote();
        testQuote.setText("test");
        testCategory = new Category();
        testCategory.setName("drama");
        testQuote.setId(Integer.MAX_VALUE);
    }

    @Test
    public void getRandomQuoteShouldReturnQuote() {
        assertNotNull(quoteDAO.getRandomQuote());
    }

    @Test
    public void getQuoteWithIdShouldReturnQuoteWhitCorrectID() throws Exception {
        assertEquals(quoteDAO.getQuoteWithId(1).getId().intValue(), 1);
    }

    @Test
    public void getQuotesOfCategoryShouldReturnWithElementForValidCategory() {
        assertTrue(quoteDAO.getQuotesOfCategory(testCategory).size() > 0);
    }

    @Test
    public void getAllQuotesShouldReturnListWithSizeOtherThanZero() {
        assertTrue(quoteDAO.getAllQuotes().size() != 0);
    }

    @Test
    public void getQuoteWithIDShouldReturnQuoteWithText() {
        Quote result = null;
        try {
            result = quoteDAO.getQuoteWithId(1);
        } catch (IOException e) {
            e.getMessage();
            e.printStackTrace();
        }
        assertTrue(result.getText() != null);
    }

    @Test(expected = IOException.class)
    public void getQuoteWithIDShouldThrowExceptionIfIDIsNotInRange() throws IOException {
        quoteDAO.getQuoteWithId(Integer.MAX_VALUE);
    }

    @Test
    public void getPendingQuotesShouldReturnWithElements() {
        testQuote.setQuoteStatus(QuoteStatus.Pending);
        quoteDAO.addQuote(testQuote);
        assertTrue(quoteDAO.getPendingQuotes().size() > 0);
    }

    @Test
    public void approveQuoteShouldSetQuoteStatusOfQuote() throws Exception {
        testQuote.setQuoteStatus(QuoteStatus.Pending);
        quoteDAO.addQuote(testQuote);
        quoteDAO.approveQuote(testQuote.getId(), "AdminTest");
        assertEquals(quoteDAO.getQuoteWithId(testQuote.getId()).getQuoteStatus(), QuoteStatus.Accepted);
    }
}