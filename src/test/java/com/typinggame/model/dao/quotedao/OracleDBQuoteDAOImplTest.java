package com.typinggame.model.dao.quotedao;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.typinggame.di.OnlineModule;
import com.typinggame.model.bean.Category;
import com.typinggame.model.bean.Quote;
import com.typinggame.model.bean.QuoteStatus;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

public class OracleDBQuoteDAOImplTest {
    private QuoteDAO quoteDAO;
    private Category testCategory;
    private Quote testQuote;

    public OracleDBQuoteDAOImplTest() {
        Injector injector = Guice.createInjector(new OnlineModule());
        testCategory = new Category();
        testCategory.setName("Poem");
        testQuote = new Quote();
        testQuote.setText("test");
        testCategory = new Category();
        testCategory.setName("Poem");
        try {
            quoteDAO = injector.getInstance(QuoteDAO.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getAllQuotesShouldReturnListWithSizeOtherThanZero() {
        assertTrue(quoteDAO.getAllQuotes().size() != 0);
    }

    @Test
    public void getAllQuotesShouldReturnWithQuotesThatHasQuoteStatusAccepted() {
        assertTrue(quoteDAO.getAllQuotes().stream().allMatch(e -> e.getQuoteStatus().equals(QuoteStatus.Accepted)));
    }

    @Test
    public void getQuoteWithIDShouldReturnQuoteWithText() {
        Quote result = null;
        try {
            result = quoteDAO.getQuoteWithId(1);
        } catch (IOException e) {
            e.getMessage();
            e.printStackTrace();
        }
        assertTrue(result.getText() != null);
    }

    @Test(expected = IOException.class)
    public void getQuoteWithIDShouldThrowExceptionIfIDIsNotInRange() throws IOException {
        quoteDAO.getQuoteWithId(Integer.MAX_VALUE);
    }

    @Test
    public void getQuotesOfCategoryShouldReturnWithElementForValidCategory() {
        assertTrue(quoteDAO.getQuotesOfCategory(testCategory).size() > 0);
    }

}