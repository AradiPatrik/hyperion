import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.typinggame.di.OfflineModule;
import com.typinggame.di.OnlineModule;
import com.typinggame.di.StringUtils;
import com.typinggame.service.routing.RoutingService;
import com.typinggame.service.routing.SceneID;
import javafx.application.Application;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.DriverManager;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private static Module getModuleBasedOnConnectivity() {
        boolean isConnectionValid = true;
        try (Connection connection = DriverManager.getConnection(StringUtils.dbURL, StringUtils.connectionName, StringUtils.connectionPassword)) {
        } catch (Exception e) {
            isConnectionValid = false;
            System.out.println("Could not connect to DB, using memory implementation!");
        }
        if (isConnectionValid) {
            return new OnlineModule();
        } else {
            return new OfflineModule();
        }
    }

    @Override
    public void start(Stage primaryStage) {
        Injector injector = Guice.createInjector(getModuleBasedOnConnectivity());
        RoutingService router = injector.getInstance(RoutingService.class);
        // TODO: 2016. 10. 19. : automate this!
        router.tryToLoadScene(SceneID.MAIN,
                Main.class.getResource("ui/scenes/menu.fxml"), injector);
        router.tryToLoadScene(SceneID.GAME,
                Main.class.getResource("ui/scenes/game.fxml"), injector);
        router.tryToLoadScene(SceneID.SETTINGS,
                Main.class.getResource("ui/scenes/settings.fxml"), injector);
        router.tryToLoadScene(SceneID.STATS,
                Main.class.getResource("ui/scenes/stats.fxml"), injector);
        router.tryToLoadScene(SceneID.QUOTE_CREATION,
                Main.class.getResource("ui/scenes/quoteCreation.fxml"), injector);
        router.tryToLoadScene(SceneID.LOG_IN,
                Main.class.getResource("ui/scenes/logIn.fxml"), injector);
        router.tryToLoadScene(SceneID.SELECT_GAME,
                Main.class.getResource("ui/scenes/selectGame.fxml"), injector);
        router.tryToLoadScene(SceneID.QUOTE_LIST,
                Main.class.getResource("ui/scenes/quoteListView.fxml"), injector);
        router.tryToLoadScene(SceneID.REGISTER,
                Main.class.getResource("ui/scenes/register.fxml"), injector);
        router.tryToLoadScene(SceneID.ADMIN_QUOTE_LIST,
                Main.class.getResource("ui/scenes/admin/quoteListView.fxml"), injector);
        router.switchScene(SceneID.LOG_IN);
    }
}
