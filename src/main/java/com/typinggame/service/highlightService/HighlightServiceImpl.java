package com.typinggame.service.highlightService;

import com.google.inject.Inject;
import com.typinggame.model.bean.UIData;
import com.typinggame.service.typinggame.CaretState;
import org.fxmisc.richtext.StyleClassedTextArea;


/**
 * Created by Patrik on 2016. 11. 15..
 */
public class HighlightServiceImpl implements HighlightService{

    private StyleClassedTextArea textArea;
    private char[] textToHighlight;
    private int lastErrorPosition;

    @Inject
    public HighlightServiceImpl(){
    }

    public void initialize(StyleClassedTextArea textArea){
        this.textArea = textArea;
        this.textToHighlight = textArea.getText().toCharArray();
        textArea.setStyleClass(0, textToHighlight.length, "default");
        textArea.setStyleClass(0, 1, "current-cursor");
    }

    public void highlightText(UIData uiData){
        if(uiData.isEnded()) {
            if (uiData.getCaretState() == CaretState.DEFAULT) {
                textArea.setStyleClass(0, textToHighlight.length, "correct-text");
            } else {
                textArea.setStyleClass(0, textToHighlight.length, "wrong-text");
            }
        }else{
            highlightNextChar(uiData);
        }
    }

    private void highlightNextChar(UIData uiData){
        if(uiData.getCaretState() == CaretState.DEFAULT) {
            handleCorrectHighlight(uiData);
        }else{
            handleMistakeHighlight(uiData);
        }
        removeFormatAfterCaret(uiData);
    }

    private void handleCorrectHighlight(UIData uiData){
        textArea.setStyleClass(0, uiData.getCaretPosition(), "correct-text");
        textArea.setStyleClass(uiData.getCaretPosition(), uiData.getCaretPosition()+1, "current-cursor");
        setPreviousCharacterCorrect();
    }

    private void setPreviousCharacterCorrect(){
        lastErrorPosition = -1;
    }

    private void handleMistakeHighlight(UIData uiData) {
        if(isPreviousCharacterCorrect()){
            lastErrorPosition = uiData.getCaretPosition()-1;
        }
        if(!isOutOfBounds(uiData.getCaretPosition())){
            textArea.setStyleClass(lastErrorPosition, uiData.getCaretPosition(), "wrong-text");
            try{
                textArea.setStyleClass(uiData.getCaretPosition(), uiData.getCaretPosition()+1, "wrong-cursor");
            }catch (IllegalArgumentException ex){

            }

        }
    }

    private boolean isPreviousCharacterCorrect(){
        return lastErrorPosition == -1;
    }

    private boolean isOutOfBounds(int caretPosition){
        return caretPosition > textToHighlight.length;
    }

    private void removeFormatAfterCaret(UIData uiData) {
        if (uiData.getCaretPosition() < textToHighlight.length)
            textArea.setStyleClass(uiData.getCaretPosition()+1, textToHighlight.length, "default");
    }

}
