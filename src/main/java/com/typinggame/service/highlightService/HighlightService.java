package com.typinggame.service.highlightService;

import com.typinggame.model.bean.UIData;
import org.fxmisc.richtext.StyleClassedTextArea;

public interface HighlightService {
    void initialize(StyleClassedTextArea textArea);
    void highlightText(UIData uiData);
}
