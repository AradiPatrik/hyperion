package com.typinggame.service.authenticationservice;

import com.typinggame.model.bean.User;
import com.typinggame.service.authenticationservice.userexception.IncompleteUserException;
import com.typinggame.service.authenticationservice.userexception.NoneUniqueUsernameException;
import com.typinggame.service.authenticationservice.userexception.UserNotFoundException;
import com.typinggame.service.authenticationservice.userexception.WrongPasswordException;

import java.util.Optional;

public interface AuthenticationService {
    Optional<User> getLoggedInUser();

    void login(String username, String password) throws UserNotFoundException, WrongPasswordException;

    void logout();

    void signUp(User user) throws IncompleteUserException, NoneUniqueUsernameException;

    boolean isUserAdmin();
}
