package com.typinggame.service.authenticationservice.userexception;

public class NoneUniqueUsernameException extends Exception {
    public NoneUniqueUsernameException() {
    }

    public NoneUniqueUsernameException(String message) {
        super(message);
    }
}
