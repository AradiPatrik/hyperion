package com.typinggame.service.authenticationservice.userexception;

public class IncompleteUserException extends Exception {
    public IncompleteUserException() {
    }

    public IncompleteUserException(String message) {
        super(message);
    }
}
