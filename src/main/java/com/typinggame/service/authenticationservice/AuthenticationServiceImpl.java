package com.typinggame.service.authenticationservice;

import com.google.inject.Inject;
import com.typinggame.model.bean.User;
import com.typinggame.model.dao.admindao.AdminDAO;
import com.typinggame.model.dao.userdao.UserDAO;
import com.typinggame.service.authenticationservice.userexception.IncompleteUserException;
import com.typinggame.service.authenticationservice.userexception.NoneUniqueUsernameException;
import com.typinggame.service.authenticationservice.userexception.UserNotFoundException;
import com.typinggame.service.authenticationservice.userexception.WrongPasswordException;

import java.util.Optional;

public class AuthenticationServiceImpl implements AuthenticationService {
    private Optional<User> user;
    private boolean isUserAdmin = false;
    private UserDAO userDAO;
    private AdminDAO adminDAO;

    @Inject
    public AuthenticationServiceImpl(UserDAO userDAO, AdminDAO adminDAO) {
        this.userDAO = userDAO;
        this.adminDAO = adminDAO;
        user = Optional.empty();
    }

    @Override
    public Optional<User> getLoggedInUser() {
        return this.user;
    }

    @Override
    public void login(String username, String password) throws UserNotFoundException, WrongPasswordException {
        if (adminDAO.authenticateAdmin(username, password)) {
            Optional<User> userFromDB = Optional.of(new User());
            userFromDB.get().setUsername(username);
            userFromDB.get().setPassword(password);
            user = userFromDB;
            this.isUserAdmin = true;
        } else {
            Optional<User> userFromDB = userDAO.getUser(username);
            if (!userFromDB.isPresent()) {
                throw new UserNotFoundException();
            } else if (!userFromDB.get().getPassword().equals(password)) {
                throw new WrongPasswordException();
            }
            this.user = userFromDB;
            this.isUserAdmin = false;
        }
    }

    @Override
    public void signUp(User user) throws IncompleteUserException, NoneUniqueUsernameException {
        if (isUserIncomplete(user)) throw new IncompleteUserException();
        if (userDAO.getUser(user.getUsername()).isPresent()) throw new NoneUniqueUsernameException();
        userDAO.addUser(user);
        try {
            login(user.getUsername(), user.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isUserAdmin() {
        return this.isUserAdmin;
    }

    private boolean isUserIncomplete(User user) {
        return user.getUsername() == null || user.getUsername().isEmpty() || user.getPassword() == null || user.getPassword().isEmpty();
    }

    @Override
    public void logout() {
        this.user = Optional.empty();
    }
}
