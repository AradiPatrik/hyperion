package com.typinggame.service.animation;

import javafx.animation.Timeline;
import javafx.scene.Node;

public interface AnimationService {

    Timeline getFadeOutAnimation(Node node);

    Timeline getFadeInAnimation(Node node);
}
