package com.typinggame.service.animation;

import com.google.inject.Inject;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.util.Duration;

/**
 * Created by User on 2016. 10. 19..
 */
public class AnimationServiceImpl implements AnimationService {

    private static final double ANIMATION_TIME = 500.0;
    private static final double OPAQUE = 1.0;
    private static final double TRANSPARENT = 0.0;

    @Inject
    public AnimationServiceImpl(){

    }

    public Timeline getFadeOutAnimation(Node node){
        return createTimeline(new KeyValue(node.opacityProperty(), OPAQUE), new KeyValue(node.opacityProperty(), TRANSPARENT));
    }

    public Timeline getFadeInAnimation(Node node){
        return createTimeline(new KeyValue(node.opacityProperty(), TRANSPARENT), new KeyValue(node.opacityProperty(), OPAQUE));
    }

    private Timeline createTimeline(KeyValue startValue, KeyValue endValue){
        KeyFrame startFrame = new KeyFrame(Duration.ZERO, startValue);
        KeyFrame endFrame = new KeyFrame(new Duration(ANIMATION_TIME), endValue);
        return new Timeline(startFrame, endFrame);
    }

}
