package com.typinggame.service.achievementService;

import com.google.inject.Inject;
import com.typinggame.model.bean.Achievement;
import com.typinggame.model.bean.User;
import com.typinggame.model.dao.achievementdao.AchievementDAO;
import com.typinggame.service.authenticationservice.AuthenticationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AchievementServiceImpl implements AchievementService {
    private AuthenticationService authenticationService;
    private AchievementDAO achievementDAO;

    @Inject
    public AchievementServiceImpl(AuthenticationService authenticationService, AchievementDAO achievementDAO) {
        this.authenticationService = authenticationService;
        this.achievementDAO = achievementDAO;
    }

    @Override
    public List<Achievement> getAllAchievements() {
        return new ArrayList<>(achievementDAO.getAllAchievement());
    }

    @Override
    public Optional<List<Achievement>> getCurrentUsersAchievements() {
        Optional<User> currentUser = authenticationService.getLoggedInUser();
        Optional<List<Achievement>> result = Optional.empty();
        if (currentUser.isPresent()) {
            result = Optional.of(achievementDAO.getAchievementsOfUser(currentUser.get()));
        }
        return result;
    }
}
