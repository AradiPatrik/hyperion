package com.typinggame.service.achievementService;

import com.typinggame.model.bean.Achievement;

import java.util.List;
import java.util.Optional;

public interface AchievementService {
    List<Achievement> getAllAchievements();

    Optional<List<Achievement>> getCurrentUsersAchievements();
}
