package com.typinggame.service.typinggame;

import com.typinggame.model.bean.UIData;
import javafx.scene.input.KeyEvent;

public interface TypingGameService {
    void initialize(char[] textToWrite);
    UIData onKeyTyped(KeyEvent keyEvent);
    GameMode getGameMode();
    void setGameMode(GameMode gameMode);
}
