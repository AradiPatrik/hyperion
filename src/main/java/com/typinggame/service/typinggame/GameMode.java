package com.typinggame.service.typinggame;

public enum GameMode {
    CustomGame,
    QuickPlay,
    SuddenDeath
}
