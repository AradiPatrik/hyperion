package com.typinggame.service.typinggame;

import com.google.inject.Inject;
import com.typinggame.model.bean.UIData;
import javafx.scene.input.KeyEvent;
import org.apache.commons.lang3.time.StopWatch;

/**
 * Created by User on 2016. 10. 16..
 */
public class TypingGameServiceImpl implements TypingGameService {

    private static final char BACKSPACE = 8;

    private enum State{
        UNSTARTED,
        STARTED,
        ENDED
    }

    private State runningState;
    private StopWatch stopWatch;
    private UIData uiData;
    private char[] textToWrite;
    private int caretPosition;
    private int correctCharsTyped;
    private int totalCharsTyped;
    private GameMode gameMode;

    @Inject
    public TypingGameServiceImpl(){
        this.stopWatch = new StopWatch();
    }

    public void initialize(char[] textToWrite){
        this.stopWatch.reset();
        this.textToWrite = textToWrite;
        this.runningState = State.UNSTARTED;
        this.uiData = new UIData();
        totalCharsTyped = 0;
        caretPosition = 0;
        correctCharsTyped = 0;
    }

    public UIData onKeyTyped(KeyEvent keyEvent) {

        if(this.runningState == State.UNSTARTED){
            stopWatch.start();
            handleInput(keyEvent);
            this.runningState = State.STARTED;
        }else if(this.runningState == State.STARTED){
            handleInput(keyEvent);
        }else if(this.runningState == State.ENDED){
            System.out.println("Text ended");
        }

        System.out.println(uiData);
        return this.uiData;
    }

    private void handleInput(KeyEvent keyEvent){

        char inputCharacter = getCharFrom(keyEvent);
        if(!Character.isISOControl(inputCharacter)){
            totalCharsTyped++;
            caretPosition++;
            calculateUIData(inputCharacter);
            checkIfEnded();
        }else if(inputCharacter == BACKSPACE && caretPosition > 0){
            caretPosition--;
            deleteCharacter();
        }
    }

    private char getCharFrom(KeyEvent keyEvent){
        return keyEvent.getCharacter().charAt(0);
    }

    private void calculateUIData(char inputCharacter){
        uiData.setCaretState(checkInput(inputCharacter));
        uiData.setAccuracy(calculateAccuracy());
        uiData.setWpm(calculateWpm());
        uiData.setCaretPosition(this.caretPosition);
    }

    private CaretState checkInput(char inputCharacter){
        if(uiData.getCaretState() == CaretState.DEFAULT && inputCharacter == textToWrite[caretPosition-1]){
            correctCharsTyped++;
            return CaretState.DEFAULT;
        } else {
            if (gameMode == GameMode.SuddenDeath) {
                this.runningState = State.ENDED;
            }
            return CaretState.ERROR;
        }
    }

    private float calculateWpm(){
        stopWatch.split();
        float wordCount = correctCharsTyped/5.0f;
        return (wordCount/(stopWatch.getSplitTime()/1000f/60f));
    }

    private float calculateAccuracy() {
        return ((float)correctCharsTyped / totalCharsTyped)*100f;
    }

    private void checkIfEnded(){
        if(this.correctCharsTyped == textToWrite.length){
            runningState = State.ENDED;
            uiData.setEnded(true);
        } else if (runningState == State.ENDED) {
            uiData.setEnded(true);
        }
    }

    private void deleteCharacter(){
        if(caretPosition == correctCharsTyped-1 && caretPosition != 0){
            correctCharsTyped--;
        }else if(uiData.getCaretState() == CaretState.ERROR){
            if (caretPosition == correctCharsTyped){
                uiData.setCaretState(CaretState.DEFAULT);
            }
        }
        uiData.setCaretPosition(caretPosition);
    }

    @Override
    public GameMode getGameMode() {
        return gameMode;
    }

    @Override
    public void setGameMode(GameMode gameMode) {
        this.gameMode = gameMode;
    }
}
