package com.typinggame.service.gameSessionService;

import com.typinggame.model.bean.GameSession;

import java.util.List;

public interface GameSessionService {
    void addGameSession(GameSession gameSession);
    List<GameSession> getGameSessionsOfUser(String username);
}
