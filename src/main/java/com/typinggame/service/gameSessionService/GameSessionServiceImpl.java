package com.typinggame.service.gameSessionService;

import com.google.inject.Inject;
import com.typinggame.model.bean.GameSession;
import com.typinggame.model.dao.gamesession.GameSessionDAO;

import java.util.List;

public class GameSessionServiceImpl implements GameSessionService {

    private GameSessionDAO gameSessionDAO;

    @Inject
    public GameSessionServiceImpl(GameSessionDAO gameSessionDAO) {
        this.gameSessionDAO = gameSessionDAO;
    }

    @Override
    public void addGameSession(GameSession gameSession) {
        gameSessionDAO.addGameSession(gameSession);
    }

    @Override
    public List<GameSession> getGameSessionsOfUser(String username) {
        return gameSessionDAO.getGameSessionOfUsername(username);
    }
}
