package com.typinggame.service.quoteService;

import com.google.inject.Inject;
import com.typinggame.model.bean.Category;
import com.typinggame.model.bean.Quote;
import com.typinggame.model.bean.User;
import com.typinggame.model.dao.quotedao.QuoteDAO;
import com.typinggame.service.authenticationservice.AuthenticationService;
import com.typinggame.service.labelsService.LabelsService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Patrik on 2016. 11. 20..
 */
public class QuoteServiceImpl implements QuoteService {
    private LabelsService labelsService;
    private Optional<Quote> gameQuote;
    private QuoteDAO quoteDAO;
    private AuthenticationService authenticationService;

    @Inject
    public QuoteServiceImpl(QuoteDAO quoteDAO,
                            AuthenticationService authenticationService,
                            LabelsService labelsService) {
        this.quoteDAO = quoteDAO;
        this.authenticationService = authenticationService;
        this.labelsService = labelsService;
    }

    @Override
    public String submit(Quote quote, int charsRemaining) {
        if (charsRemaining < 0) {
            return labelsService.getString("tooManyCharacters");
        } else if (!authenticationService.getLoggedInUser().isPresent()) {
            return labelsService.getString("uploaderNotLoggedIn");
        } else if (quote.getTitle().length() == 0) {
            return labelsService.getString("titleMissing");
        } else if (quote.getAuthor().length() == 0) {
            return labelsService.getString("authorMissing");
        } else if (quote.getText().length() < 50) {
            return labelsService.getString("notEnoughCharacters");
        } else {
            quote.setUploaderUsername(authenticationService.getLoggedInUser().get().getUsername());
            quoteDAO.addQuote(quote);
        }
        return labelsService.getString("submitSuccessful");
    }

    @Override
    public Optional<Quote> getGameQuote() {
        return gameQuote;
    }

    @Override
    public void setGameQuote(Quote gameQuote) {
        this.gameQuote = Optional.of(gameQuote);
    }

    @Override
    public List<Quote> getQuotes() {
        return new ArrayList<>(quoteDAO.getAllQuotes());
    }

    @Override
    public void setGameQuoteToRandom() {
        gameQuote = Optional.of(quoteDAO.getRandomQuote());
    }

    @Override
    public List<Quote> getQuotesOfCategory(Category category) {
        return new ArrayList<>(quoteDAO.getQuotesOfCategory(category));
    }

    @Override
    public List<Quote> getPendingQuotes() {
        return new ArrayList<>(quoteDAO.getPendingQuotes());
    }

    @Override
    public void approveQuote(Quote quote, User admin) throws IOException {
        quoteDAO.approveQuote(quote.getId(), admin.getUsername());
    }


}
