package com.typinggame.service.quoteService;

import com.typinggame.model.bean.Category;
import com.typinggame.model.bean.Quote;
import com.typinggame.model.bean.User;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface QuoteService {
    String submit(Quote quote, int charsRemaining);

    Optional<Quote> getGameQuote();

    void setGameQuote(Quote gameQuote);

    List<Quote> getQuotes();

    void setGameQuoteToRandom();

    List<Quote> getQuotesOfCategory(Category category);

    List<Quote> getPendingQuotes();

    void approveQuote(Quote quote, User admin) throws IOException;
}
