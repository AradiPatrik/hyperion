package com.typinggame.service.component;

public interface ComponentService {
    QuoteComponent getQuoteComponent();
    AdminQuoteComponent getAdminQuoteComponent();
}
