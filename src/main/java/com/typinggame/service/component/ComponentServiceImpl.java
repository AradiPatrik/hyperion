package com.typinggame.service.component;

import com.google.inject.Inject;
import com.typinggame.controller.QuoteItemController;
import com.typinggame.service.labelsService.LabelsService;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

public class ComponentServiceImpl implements ComponentService {

    private LabelsService labelsService;

    // TODO: 5/4/2018 What is happening here is BAD, remove code duplication
    @Inject
    public ComponentServiceImpl(LabelsService labelsService) {
        this.labelsService = labelsService;
    }

    @Override
    public QuoteComponent getQuoteComponent() {
        QuoteComponent component = new QuoteComponent();
        URL resourceUrl = getClass().getResource("/ui/scenes/quoteView.fxml");
        System.out.println(resourceUrl);
        FXMLLoader loader = new FXMLLoader(resourceUrl, labelsService.getLabels());
        try {
            component.view = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        component.controller = loader.getController();
        return component;
    }

    @Override
    public AdminQuoteComponent getAdminQuoteComponent() {
        AdminQuoteComponent component = new AdminQuoteComponent();
        URL resourceUrl = getClass().getResource("/ui/scenes/admin/quoteView.fxml");
        System.out.println(resourceUrl);
        FXMLLoader loader = new FXMLLoader(resourceUrl, labelsService.getLabels());
        try {
            component.view = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        component.controller = loader.getController();
        return component;
    }
}
