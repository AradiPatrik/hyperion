package com.typinggame.service.labelsService;

import java.util.ResourceBundle;

public interface LabelsService {
    String getString(String key);

    ResourceBundle getLabels();
}
