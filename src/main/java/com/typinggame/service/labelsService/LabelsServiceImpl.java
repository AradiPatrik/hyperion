package com.typinggame.service.labelsService;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import java.util.Locale;
import java.util.ResourceBundle;

public class LabelsServiceImpl implements LabelsService {
    private ResourceBundle labels;

    @Inject
    public LabelsServiceImpl(@Named("BoundleURL") String boundleURL, @Named("Locale") Locale locale) {
        this.labels = ResourceBundle.getBundle(boundleURL, locale);
    }

    @Override
    public String getString(String key) {
        return labels.getString(key);
    }

    @Override
    public ResourceBundle getLabels() {
        return this.labels;
    }
}
