package com.typinggame.service.routing;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.typinggame.controller.Manageable;
import com.typinggame.service.animation.AnimationService;
import com.typinggame.service.labelsService.LabelsService;
import javafx.animation.Timeline;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by User on 2016. 10. 15..
 */
public class RoutingServiceImpl implements RoutingService {

    private HashMap<SceneID, Component> components = new HashMap<>();
    private Dimension dimensions;
    private AnimationService animationService;
    private Stage stage;
    private LabelsService labelsService;

    @Inject
    public RoutingServiceImpl(AnimationService animationService, LabelsService labelsService) {
        this.labelsService = labelsService;
        this.stage = new Stage();
        this.stage.getIcons().add(new Image("ui/icon/typewriter.png"));
        this.animationService = animationService;
        dimensions = Toolkit.getDefaultToolkit().getScreenSize();

    }

    private void addScene(SceneID sceneID, Component component) {
        components.put(sceneID, component);
    }

    @Override
    public void tryToLoadScene(SceneID sceneID, URL resourceUrl, Injector injector) {
        try {
            loadScene(sceneID, resourceUrl, injector);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void close() {
        stage.close();
    }

    private void loadScene(SceneID sceneID, URL resourceUrl, Injector injector) throws IOException {
        // TODO: 3/5/2018 split view initialization and controller initialization
        System.out.println(resourceUrl);
        FXMLLoader fxmlLoader = new FXMLLoader(resourceUrl, labelsService.getLabels());
        fxmlLoader.setControllerFactory(injector::getInstance);
        Parent root = fxmlLoader.load();
        Manageable sceneController = fxmlLoader.getController();
        sceneController.setRoutingService(this);
        Scene scene = new Scene(root);
        addScene(sceneID, new Component(scene, sceneController));
    }

    @Override
    public void switchScene(SceneID sceneID) {
        if (!isSceneCached(sceneID)) throw new IllegalArgumentException("The scene was not loaded.");
        Component componentToLoad = components.get(sceneID);
        if (!isFirstScene()) {
            Timeline fadeOutAnimation = animationService.getFadeOutAnimation(stage.getScene().getRoot());
            fadeOutAnimation.play();
            fadeOutAnimation.setOnFinished(e -> fadeInNextScene(componentToLoad.scene));
        } else {
            stage.setScene(componentToLoad.scene);
            stage.show();
            animationService.getFadeInAnimation(stage.getScene().getRoot()).play();
        }
        componentToLoad.controller.onSceneSwitch();
    }

    private boolean isSceneCached(SceneID sceneID) {
        return components.get(sceneID) != null;
    }

    private boolean isFirstScene() {
        return stage.getScene() == null;
    }

    private void fadeInNextScene(Scene nextScene) {
        stage.setScene(nextScene);
        Timeline fadeIn = animationService.getFadeInAnimation(stage.getScene().getRoot());
        fadeIn.play();
        centerStage();
    }

    private void centerStage() {
        this.stage.setX((dimensions.getWidth() / 2.0) - (this.stage.getWidth() / 2.0));
        this.stage.setY((dimensions.getHeight() / 2.0) - (this.stage.getHeight() / 2.0));
        System.out.println(stage.getWidth());
    }

    @Override
    public boolean unloadScene(SceneID sceneID) {
        if (components.remove(sceneID) == null) {
            System.out.println("Screen didn't exist");
            return false;
        } else {
            return true;
        }
    }


}
