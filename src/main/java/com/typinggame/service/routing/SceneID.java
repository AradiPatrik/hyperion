package com.typinggame.service.routing;

/**
 * Created by User on 2016. 10. 15..
 */
public enum SceneID {


    MAIN,
    GAME,
    SETTINGS,
    STATS,
    QUOTE_CREATION,
    LOG_IN,
    SELECT_GAME,
    QUOTE_LIST,
    REGISTER,
    ADMIN_QUOTE_LIST

}
