package com.typinggame.service.routing;

import com.typinggame.controller.Manageable;
import javafx.scene.Scene;

public class Component {
    public Scene scene;
    public Manageable controller;

    public Component(Scene scene, Manageable controller) {
        this.scene = scene;
        this.controller = controller;
    }
}
