package com.typinggame.service.routing;

import com.google.inject.Injector;

import java.net.URL;

public interface RoutingService {
    void tryToLoadScene(SceneID sceneID, URL resourceUrl, Injector injector);
    void switchScene(SceneID sceneID);
    boolean unloadScene(SceneID sceneID);
    void close();
}
