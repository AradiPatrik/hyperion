package com.typinggame.di;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.typinggame.controller.*;
import com.typinggame.model.dao.achievementdao.AchievementDAO;
import com.typinggame.model.dao.achievementdao.MemAchievementDAOImpl;
import com.typinggame.model.dao.gamesession.GameSessionDAO;
import com.typinggame.model.dao.gamesession.MemGameSessionDAOImpl;
import com.typinggame.model.dao.quotedao.MemQuoteDAOImpl;
import com.typinggame.model.dao.quotedao.QuoteDAO;
import com.typinggame.model.dao.userdao.MemUserDAOImpl;
import com.typinggame.model.dao.userdao.UserDAO;
import com.typinggame.service.achievementService.AchievementService;
import com.typinggame.service.achievementService.AchievementServiceImpl;
import com.typinggame.service.animation.AnimationService;
import com.typinggame.service.animation.AnimationServiceImpl;
import com.typinggame.service.authenticationservice.AuthenticationService;
import com.typinggame.service.authenticationservice.AuthenticationServiceImpl;
import com.typinggame.service.component.ComponentService;
import com.typinggame.service.component.ComponentServiceImpl;
import com.typinggame.service.gameSessionService.GameSessionService;
import com.typinggame.service.gameSessionService.GameSessionServiceImpl;
import com.typinggame.service.highlightService.HighlightService;
import com.typinggame.service.highlightService.HighlightServiceImpl;
import com.typinggame.service.labelsService.LabelsService;
import com.typinggame.service.labelsService.LabelsServiceImpl;
import com.typinggame.service.quoteService.QuoteService;
import com.typinggame.service.quoteService.QuoteServiceImpl;
import com.typinggame.service.routing.RoutingService;
import com.typinggame.service.routing.RoutingServiceImpl;
import com.typinggame.service.typinggame.TypingGameService;
import com.typinggame.service.typinggame.TypingGameServiceImpl;

import java.util.Locale;

public class OfflineModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(SelectGameController.class);
        bind(MenuController.class);
        bind(QuoteCreationController.class);
        bind(SettingsController.class);
        bind(StatsController.class);
        bind(GameController.class);
        bind(RegisterController.class);
        bind(AdminQuoteListController.class);
        bind(AchievementService.class).to(AchievementServiceImpl.class).in(Singleton.class);
        bind(RoutingService.class).to(RoutingServiceImpl.class).in(Singleton.class);
        bind(AnimationService.class).to(AnimationServiceImpl.class).in(Singleton.class);
        bind(HighlightService.class).to(HighlightServiceImpl.class).in(Singleton.class);
        bind(QuoteService.class).to(QuoteServiceImpl.class).in(Singleton.class);
        bind(TypingGameService.class).to(TypingGameServiceImpl.class).in(Singleton.class);
        bind(QuoteDAO.class).to(MemQuoteDAOImpl.class).in(Singleton.class);
        bind(ComponentService.class).to(ComponentServiceImpl.class).in(Singleton.class);
        bind(UserDAO.class).to(MemUserDAOImpl.class).in(Singleton.class);
        bind(AchievementDAO.class).to(MemAchievementDAOImpl.class).in(Singleton.class);
        bind(AuthenticationService.class).to(AuthenticationServiceImpl.class).in(Singleton.class);
        bind(GameSessionDAO.class).to(MemGameSessionDAOImpl.class).in(Singleton.class);
        bind(GameSessionService.class).to(GameSessionServiceImpl.class).in(Singleton.class);
        bind(String.class)
                .annotatedWith(Names.named("BoundleURL"))
                .toInstance(StringUtils.boundleURL);
        bind(LabelsService.class).to(LabelsServiceImpl.class).in(Singleton.class);
        bind(Locale.class)
                .annotatedWith(Names.named("Locale"))
                .toProvider(LocaleProvider.class);

    }
}
