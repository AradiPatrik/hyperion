package com.typinggame.di;

import com.google.inject.Provider;

import java.util.Locale;

public class LocaleProvider implements Provider<Locale> {
    @Override
    public Locale get() {
        //TODO read this from config file!
        return Locale.getDefault();
    }
}
