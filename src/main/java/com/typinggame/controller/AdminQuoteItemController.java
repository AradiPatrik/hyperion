package com.typinggame.controller;

import com.typinggame.model.bean.Quote;
import com.typinggame.service.authenticationservice.AuthenticationService;
import com.typinggame.service.quoteService.QuoteService;
import com.typinggame.service.routing.RoutingService;
import com.typinggame.service.routing.SceneID;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.io.IOException;

public class AdminQuoteItemController implements Manageable {

    private RoutingService routingService;
    private QuoteService quoteService;
    private VBox quoteContainer;
    private AuthenticationService authService;

    @FXML
    private Label quoteHeader;
    @FXML
    private Text quoteText;
    @FXML
    private VBox root;

    private Quote quote;

    @Override
    public void setRoutingService(RoutingService routingService) {
        this.routingService = routingService;
    }

    @Override
    public void onSceneSwitch() {

    }

    public void setQuoteService(QuoteService quoteService) {
        this.quoteService = quoteService;
    }

    public void setQuoteContainer(VBox quoteContainer) {
        this.quoteContainer = quoteContainer;
    }

    public void setAuthService(AuthenticationService authService) {
        this.authService = authService;
    }

    public void setQuote(Quote quote) {
        this.quote = quote;
        updateView();
    }

    private void updateView() {
        StringBuilder quoteHeaderBuilder = new StringBuilder();
        quoteHeaderBuilder.append(quote.getAuthor())
                .append(" - ")
                .append(quote.getTitle());
        quoteHeader.setText(quoteHeaderBuilder.toString());
        quoteText.setText(quote.getText());
    }

    public void onDecline(ActionEvent actionEvent) {
        quoteContainer.getChildren().remove(root);
    }

    public void onApprove(ActionEvent actionEvent) {
        quoteContainer.getChildren().remove(root);
        if (authService.getLoggedInUser().isPresent()) {
            try {
                quoteService.approveQuote(quote, authService.getLoggedInUser().get());
            } catch (IOException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException("could not approve quote");
            }
        } else {
            throw new RuntimeException("onApprove without a logged in user");
        }
    }
}
