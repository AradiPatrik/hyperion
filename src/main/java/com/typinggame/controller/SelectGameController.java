package com.typinggame.controller;

import com.google.inject.Inject;
import com.typinggame.service.quoteService.QuoteService;
import com.typinggame.service.routing.RoutingService;
import com.typinggame.service.routing.SceneID;
import com.typinggame.service.typinggame.GameMode;
import com.typinggame.service.typinggame.TypingGameService;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public class SelectGameController implements Initializable, Manageable {
    private QuoteService quoteService;
    private RoutingService routingService;
    private TypingGameService typingGameService;

    @Inject
    public SelectGameController(QuoteService quoteService, TypingGameService typingGameService) {
        this.quoteService = quoteService;
        this.typingGameService = typingGameService;
    }


    @Override
    public void setRoutingService(RoutingService routingService) {
        this.routingService = routingService;
    }

    @Override
    public void onSceneSwitch() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void goToMain() {
        routingService.switchScene(SceneID.MAIN);
    }

    public void quickGame() {
        quoteService.setGameQuoteToRandom();
        typingGameService.setGameMode(GameMode.QuickPlay);
        routingService.switchScene(SceneID.GAME);
    }

    public void customGame() {
        typingGameService.setGameMode(GameMode.CustomGame);
        routingService.switchScene(SceneID.QUOTE_LIST);
    }

    public void suddenDeath(ActionEvent actionEvent) {
        System.out.println("Click");
        quoteService.setGameQuoteToRandom();
        typingGameService.setGameMode(GameMode.SuddenDeath);
        routingService.switchScene(SceneID.GAME);
    }
}
