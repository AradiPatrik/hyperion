package com.typinggame.controller;

import com.typinggame.model.bean.Quote;
import com.typinggame.service.quoteService.QuoteService;
import com.typinggame.service.routing.RoutingService;
import com.typinggame.service.routing.SceneID;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.text.Text;

public class QuoteItemController implements Manageable {

    @FXML
    private Label quoteHeader;
    @FXML
    private Text quoteText;
    private RoutingService routingService;
    private QuoteService quoteService;

    private Quote quote;

    @Override
    public void setRoutingService(RoutingService routingService) {
        this.routingService = routingService;
    }

    @Override
    public void onSceneSwitch() {

    }

    public void setQuoteService(QuoteService quoteService) {
        this.quoteService = quoteService;
    }

    public void setQuote(Quote quote) {
        this.quote = quote;
        updateView();
    }

    private void updateView() {
        StringBuilder quoteHeaderBuilder = new StringBuilder();
        quoteHeaderBuilder.append(quote.getAuthor())
                .append(" - ")
                .append(quote.getTitle());
        quoteHeader.setText(quoteHeaderBuilder.toString());
        quoteText.setText(quote.getText());
    }

    public void onPracticeOnThisQuoteClick(ActionEvent actionEvent) {
        if (quoteService == null) {
            throw new RuntimeException("You must set the quote service for a quote item");
        }
        if (routingService == null) {
            throw new RuntimeException("You must set the routing service for a manageable item");
        }
        quoteService.setGameQuote(quote);
        routingService.switchScene(SceneID.GAME);
    }
}
