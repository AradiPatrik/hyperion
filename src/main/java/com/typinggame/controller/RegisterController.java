package com.typinggame.controller;

import com.google.inject.Inject;
import com.typinggame.model.bean.User;
import com.typinggame.service.animation.AnimationService;
import com.typinggame.service.authenticationservice.AuthenticationService;
import com.typinggame.service.authenticationservice.userexception.IncompleteUserException;
import com.typinggame.service.authenticationservice.userexception.NoneUniqueUsernameException;
import com.typinggame.service.labelsService.LabelsService;
import com.typinggame.service.routing.RoutingService;
import com.typinggame.service.routing.SceneID;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.sql.Date;
import java.time.Instant;


public class RegisterController implements Manageable {

    private RoutingService routingService;
    private AuthenticationService authenticationService;
    private LabelsService labelsService;
    private AnimationService animationService;

    @FXML
    private TextField usernameTextField;
    @FXML
    private PasswordField passwordTextField;
    @FXML
    private PasswordField passwordAgainTextField;
    @FXML
    private Label messageLabel;

    @Inject
    public RegisterController(AuthenticationService authenticationService, LabelsService labelsService, AnimationService animationService) {
        this.authenticationService = authenticationService;
        this.labelsService = labelsService;
        this.animationService = animationService;
    }

    public void register(ActionEvent actionEvent) {
        if (passwordAgainTextField.getText().equals(passwordTextField.getText())) {
            try {
                registerUser();
                displayMessage(labelsService.getString("registerSuccessful"));
                assert (authenticationService.getLoggedInUser().isPresent());
                routingService.switchScene(SceneID.MAIN);
            } catch (IncompleteUserException e) {
                displayMessage(labelsService.getString("incompleteUserError"));
            } catch (NoneUniqueUsernameException e) {
                displayMessage(labelsService.getString("nonUniqueUsernameError"));
            }
        } else {
            displayMessage(labelsService.getString("samePasswordError"));
        }
    }

    void displayMessage(String message) {
        if (messageLabel.opacityProperty().getValue() < 1.0) {
            messageLabel.setText(message);
            Timeline animation = animationService.getFadeInAnimation(messageLabel);
            animation.play();
        } else {
            Timeline animation = animationService.getFadeOutAnimation(messageLabel);
            animation.setOnFinished((e) -> {
                messageLabel.setText(message);
                animationService.getFadeInAnimation(messageLabel).play();
            });
            animation.play();
        }
    }

    private void registerUser() throws IncompleteUserException, NoneUniqueUsernameException {
        User newUser = new User();
        newUser.setPassword(passwordTextField.getText());
        newUser.setRegisterDate(Date.from(Instant.now()));
        newUser.setUsername(usernameTextField.getText());
        authenticationService.signUp(newUser);
    }

    public void playAsGuest(ActionEvent actionEvent) {
        routingService.switchScene(SceneID.MAIN);
    }

    public void quit(ActionEvent actionEvent) {
        routingService.close();
    }

    @Override
    public void setRoutingService(RoutingService routingService) {
        this.routingService = routingService;
    }

    @Override
    public void onSceneSwitch() {

    }
}
