package com.typinggame.controller;

import com.google.inject.Inject;
import com.typinggame.model.bean.Quote;
import com.typinggame.service.animation.AnimationService;
import com.typinggame.service.quoteService.QuoteService;
import com.typinggame.service.routing.RoutingService;
import com.typinggame.service.routing.SceneID;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 * Created by Patrik on 2016. 11. 20..
 */
public class QuoteCreationController implements Manageable {

    private static final int MAX_CHARACTERS = 500;

    public TextArea textArea;
    public Label counter;
    public TextField authorField;
    public TextField titleField;
    public Label messageLabel;
    private RoutingService routingService;
    private QuoteService quoteService;
    private AnimationService animationService;


    @Inject
    public QuoteCreationController(QuoteService quoteService, AnimationService animationService) {
        this.quoteService = quoteService;
        this.animationService = animationService;
    }

    @Override
    public void setRoutingService(RoutingService routingService) {
        this.routingService = routingService;
    }

    @Override
    public void onSceneSwitch() {

    }

    public void onKeyTyped(KeyEvent keyEvent) {
        int remainingChars = textArea.getText().length();
        counter.setText(String.valueOf(MAX_CHARACTERS - remainingChars));
    }

    public void submit(ActionEvent actionEvent) {
        // TODO: 3/28/2018 handle categories
        Quote quoteToSubmit =
                Quote.fromQuoteSubmissionData(authorField.getText(), titleField.getText(), textArea.getText(), "patrik", null);
        System.out.println(titleField.getText().length());
        String message = quoteService.submit(quoteToSubmit, Integer.parseInt(counter.getText()));
        showMessage(message);
    }

    private void showMessage(String message) {
        messageLabel.setText(message);
        animationService.getFadeInAnimation(messageLabel).play();

    }

    public void backToMain(ActionEvent actionEvent) {
        routingService.switchScene(SceneID.MAIN);
    }
}
