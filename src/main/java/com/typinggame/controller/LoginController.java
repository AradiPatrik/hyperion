package com.typinggame.controller;

import com.google.inject.Inject;
import com.typinggame.service.authenticationservice.AuthenticationService;
import com.typinggame.service.authenticationservice.userexception.UserNotFoundException;
import com.typinggame.service.authenticationservice.userexception.WrongPasswordException;
import com.typinggame.service.labelsService.LabelsService;
import com.typinggame.service.routing.RoutingService;
import com.typinggame.service.routing.SceneID;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController implements Manageable {

    private AuthenticationService authenticationService;
    private LabelsService labelsService;
    private RoutingService routingService;

    @FXML
    private Label message;
    @FXML
    private TextField usernameTextField;
    @FXML
    private PasswordField passwordTextField;

    @Override
    public void setRoutingService(RoutingService routingService) {
        this.routingService = routingService;
    }

    @Override
    public void onSceneSwitch() {

    }

    @Inject
    public LoginController(AuthenticationService authenticationService, LabelsService labelsService) {
        this.authenticationService = authenticationService;
        this.labelsService = labelsService;
    }

    public void logIn(ActionEvent actionEvent) {
        try {
            logInUser();
        } catch (UserNotFoundException e) {
            message.setText(labelsService.getString("userNotFoundMessage"));
        } catch (WrongPasswordException e) {
            message.setText(labelsService.getString("wrongPasswordMessage"));
        }
    }

    private void logInUser() throws UserNotFoundException, WrongPasswordException {
        authenticationService.login(usernameTextField.getText(), passwordTextField.getText());
        if (authenticationService.getLoggedInUser().isPresent()) {
            System.out.println(authenticationService.getLoggedInUser().get());
        } else {
            System.out.println("User is not present after login panicing");
            throw new RuntimeException();
        }
        if (authenticationService.isUserAdmin()) {
            routingService.switchScene(SceneID.ADMIN_QUOTE_LIST);
        } else {
            routingService.switchScene(SceneID.MAIN);
        }
    }

    public void signUp(ActionEvent actionEvent) {
        routingService.switchScene(SceneID.REGISTER);
    }

    public void playAsGuest(ActionEvent actionEvent) {
        routingService.switchScene(SceneID.MAIN);
    }

}
