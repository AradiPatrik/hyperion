package com.typinggame.controller;

import com.google.inject.Inject;
import com.typinggame.model.bean.Quote;
import com.typinggame.model.bean.UIData;
import com.typinggame.service.highlightService.HighlightService;
import com.typinggame.service.quoteService.QuoteService;
import com.typinggame.service.routing.RoutingService;
import com.typinggame.service.routing.SceneID;
import com.typinggame.service.typinggame.GameMode;
import com.typinggame.service.typinggame.TypingGameService;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import org.fxmisc.richtext.StyleClassedTextArea;

/**
 * Created by User on 2016. 10. 15..
 */
public class GameController implements Manageable {


    private RoutingService routingService;
    private TypingGameService typingGameService;
    private HighlightService highlightService;
    private char[] textToWrite;
    private Quote quote;
    private QuoteService quoteService;
    private String currentLine;

    @FXML
    private TextField inputField;
    @FXML
    private StyleClassedTextArea textArea;
    @FXML
    private Label wpmDisplay;
    @FXML
    private Label accuracyDisplay;
    @FXML
    private Button newButton;

    @Inject
    public GameController(TypingGameService typingGameService,
                          HighlightService highlightService,
                          QuoteService quoteService) {
        this.typingGameService = typingGameService;
        this.highlightService = highlightService;
        this.quoteService = quoteService;
    }

    @Override
    public void setRoutingService(RoutingService routingService) {
        this.routingService = routingService;
    }

    @Override
    public void onSceneSwitch() {
        if (typingGameService.getGameMode() == GameMode.CustomGame) {
            newButton.setVisible(false);
        }
        reset();
    }

    public void goToMain(ActionEvent actionEvent) {
        routingService.switchScene(SceneID.MAIN);
    }

    public void onResetClick() {
        reset();
    }

    private void reset() {
        quoteService.getGameQuote().ifPresent(e -> this.quote = e);
        currentLine = quote.getText();
        textArea.replaceText(currentLine);
        textArea.getText().toCharArray();
        textToWrite = textArea.getText().toCharArray();
        typingGameService.initialize(textToWrite);
        typingGameService.initialize(textToWrite);
        inputField.clear();
        wpmDisplay.setText("-");
        accuracyDisplay.setText("-");
        highlightService.initialize(textArea);
        inputField.setDisable(false);
        Platform.runLater(() -> inputField.requestFocus());
    }

    public void onKeyTyped(KeyEvent keyEvent) {
        UIData uiData = typingGameService.onKeyTyped(keyEvent);
        updateDisplay(uiData);
        if (uiData.isEnded()) {
            inputField.setDisable(true);
        }
    }

    private void updateDisplay(UIData uiData) {
        updateWPM(uiData.getWpm());
        updateAccuracy(uiData.getAccuracy());
        highlightSentence(uiData);
    }

    private void updateWPM(float wpm) {
        String wpmToWrite = String.format("%.2f", wpm);
        wpmDisplay.setText(wpmToWrite);
    }

    private void updateAccuracy(float accuracy) {
        String accToWrite = String.format("%.2f %%", accuracy);
        accuracyDisplay.setText(accToWrite);
    }

    private void highlightSentence(UIData uiData) {
        highlightService.highlightText(uiData);
    }


    public void onNewButtonClick(ActionEvent actionEvent) {
        System.out.println("onNewButtonClick");
        if (typingGameService.getGameMode() == GameMode.CustomGame) {
            System.out.println("This shouldn't have happend");
        } else {
            System.out.println("getting random quote");
            quoteService.setGameQuoteToRandom();
            reset();
        }
    }

    public void onLikeClick(ActionEvent actionEvent) {
    }

    public void onDislikeClick(ActionEvent actionEvent) {
    }
}
