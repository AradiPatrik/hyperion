package com.typinggame.controller;

import com.google.inject.Inject;
import com.typinggame.service.routing.RoutingService;
import com.typinggame.service.routing.SceneID;
import com.typinggame.service.routing.RoutingServiceImpl;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public class SettingsController implements Initializable, Manageable {

    private RoutingService routingService;

    @Inject
    public SettingsController(){

    }

    @Override
    public void setRoutingService(RoutingService routingService) {
        this.routingService = routingService;
    }

    @Override
    public void onSceneSwitch() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //TODO
    }

    public void goToMain(ActionEvent actionEvent) {
        routingService.switchScene(SceneID.MAIN);
    }
}
