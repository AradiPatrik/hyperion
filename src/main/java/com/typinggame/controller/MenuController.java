package com.typinggame.controller;

import com.google.inject.Inject;
import com.typinggame.service.routing.RoutingService;
import com.typinggame.service.routing.SceneID;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public class MenuController implements Manageable {

    private RoutingService routingService;

    @Inject
    public MenuController() {

    }

    @Override
    public void setRoutingService(RoutingService routingService) {
        this.routingService = routingService;
    }

    @Override
    public void onSceneSwitch() {

    }


    public void selectGame(ActionEvent actionEvent) {
        routingService.switchScene(SceneID.SELECT_GAME);
    }

    public void exitGame(ActionEvent actionEvent) {
        routingService.close();
    }

    public void goToSettings(ActionEvent actionEvent) {
        routingService.switchScene(SceneID.SETTINGS);
    }

    public void goToStats(ActionEvent actionEvent) {
        routingService.switchScene(SceneID.STATS);
    }

    public void goToCodeCreation(ActionEvent actionEvent) {
        routingService.switchScene(SceneID.QUOTE_CREATION);
    }
}
