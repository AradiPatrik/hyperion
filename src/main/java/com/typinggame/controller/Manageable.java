package com.typinggame.controller;

import com.typinggame.service.routing.RoutingService;

/**
 * Created by User on 2016. 10. 15..
 */
public interface Manageable {
    void setRoutingService(RoutingService routingService);
    void onSceneSwitch();
}
