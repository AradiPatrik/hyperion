package com.typinggame.controller;

import com.typinggame.model.bean.Quote;
import com.typinggame.service.component.AdminQuoteComponent;
import com.typinggame.service.component.ComponentService;
import com.typinggame.service.component.QuoteComponent;
import com.typinggame.service.quoteService.QuoteService;
import com.typinggame.service.routing.RoutingService;
import com.typinggame.service.routing.SceneID;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class AdminQuoteListController implements Manageable {

    @FXML
    private VBox quotesVBox;
    @FXML
    private ScrollPane quoteScrollPane;


    private RoutingService routingService;
    private QuoteService quoteService;
    private ComponentService componentService;

    private List<Quote> quotes = new ArrayList<>();

    @Override
    public void setRoutingService(RoutingService routingService) {
        this.routingService = routingService;
    }

    @Override
    public void onSceneSwitch() {

    }

    @Inject
    public AdminQuoteListController(ComponentService componentService, QuoteService quoteService) {
        this.componentService = componentService;
        this.quoteService = quoteService;
    }

    public void onApplyFilterClick(ActionEvent actionEvent) {
        quotes = quoteService.getQuotes();
        quotes.stream().limit(5)
                .forEach(this::addQuoteToView);
        System.out.println("success");
    }

    private void addQuoteToView(Quote quote) {
        AdminQuoteComponent quoteComponent = componentService.getAdminQuoteComponent();
        quoteComponent.controller.setQuoteService(quoteService);
        quotesVBox.getChildren().add(quoteComponent.view);
        quoteComponent.controller.setQuote(quote);
        quoteComponent.controller.setRoutingService(routingService);
        quoteComponent.controller.setQuoteContainer(quotesVBox);
    }

    public void onPrevClick(ActionEvent actionEvent) {

    }

    public void onNextClick(ActionEvent actionEvent) {

    }

    public void onExitClick(ActionEvent actionEvent) {
        routingService.switchScene(SceneID.MAIN);
    }
}
