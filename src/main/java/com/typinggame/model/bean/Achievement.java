package com.typinggame.model.bean;

import java.util.Objects;

public class Achievement {
    private String name;
    private Integer pointValue;

    public Achievement() {

    }

    public Achievement(String name, Integer pointValue) {
        this.name = name;
        this.pointValue = pointValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPointValue() {
        return pointValue;
    }

    public void setPointValue(Integer pointValue) {
        this.pointValue = pointValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Achievement that = (Achievement) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(pointValue, that.pointValue);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, pointValue);
    }

    @Override
    public String toString() {
        return "Achievement{" +
                "name='" + name + '\'' +
                ", pointValue=" + pointValue +
                '}';
    }
}
