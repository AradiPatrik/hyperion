package com.typinggame.model.bean;

import java.util.Date;

public class GameSession {

    private double wpm;
    private double accuracy;
    private Date date;
    private int quoteID;
    private String username;

    public GameSession() {

    }

    public GameSession(double wpm, double accuracy, Date date, int quoteID, String username) {
        this.wpm = wpm;
        this.accuracy = accuracy;
        this.date = date;
        this.quoteID = quoteID;
        this.username = username;
    }

    public double getWpm() {
        return wpm;
    }

    public void setWpm(double wpm) {
        this.wpm = wpm;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameSession that = (GameSession) o;

        if (Double.compare(that.wpm, wpm) != 0) return false;
        if (Double.compare(that.accuracy, accuracy) != 0) return false;
        return date != null ? date.equals(that.date) : that.date == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(wpm);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(accuracy);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "GameSession{" +
                "wpm=" + wpm +
                ", accuracy=" + accuracy +
                ", date=" + date +
                '}';
    }

    public int getQuoteID() {
        return quoteID;
    }

    public void setQuoteID(int quoteID) {
        this.quoteID = quoteID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
