package com.typinggame.model.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by User on 2016. 10. 16..
 */
public class Quote implements Serializable {

    private Integer id;
    private String author;
    private String title;
    private String text;
    private String uploaderUsername;
    private String approvedByAdminUsername;
    private Date uploadDate;
    private List<Category> categories;
    private Double rating;
    private QuoteStatus quoteStatus;

    public Quote() {
    }

    public Quote(Integer id, String author, String title, String text, String uploaderUsername, Date uploadDate, List<Category> categories, QuoteStatus quoteStatus) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.text = text;
        this.uploaderUsername = uploaderUsername;
        this.uploadDate = uploadDate;
        this.categories = categories;
        this.quoteStatus = quoteStatus;
    }

    public static Quote fromQuoteSubmissionData(String author, String title, String text, String uploaderUsername, List<Category> categories) {
        Quote quote = new Quote();
        quote.author = author;
        quote.title = title;
        quote.text = text;
        quote.uploaderUsername = uploaderUsername;
        quote.id = null;
        quote.uploadDate = null;
        quote.categories = categories;
        return quote;
    }

    public QuoteStatus getQuoteStatus() {
        return quoteStatus;
    }

    public void setQuoteStatus(QuoteStatus quoteStatus) {
        this.quoteStatus = quoteStatus;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getApprovedByAdminUsername() {
        return approvedByAdminUsername;
    }

    public void setApprovedByAdminUsername(String approvedByAdminUsername) {
        this.approvedByAdminUsername = approvedByAdminUsername;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUploaderUsername() {
        return uploaderUsername;
    }

    public void setUploaderUsername(String uploaderUsername) {
        this.uploaderUsername = uploaderUsername;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Quote quote = (Quote) o;
        return Objects.equals(id, quote.id) &&
                Objects.equals(author, quote.author) &&
                Objects.equals(title, quote.title) &&
                Objects.equals(text, quote.text) &&
                Objects.equals(uploaderUsername, quote.uploaderUsername) &&
                Objects.equals(approvedByAdminUsername, quote.approvedByAdminUsername) &&
                Objects.equals(uploadDate, quote.uploadDate) &&
                Objects.equals(categories, quote.categories) &&
                Objects.equals(rating, quote.rating);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, author, title, text, uploaderUsername, approvedByAdminUsername, uploadDate, categories, rating);
    }

    @Override
    public String toString() {
        return "Quote{" +
                "id=" + id +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", uploaderUsername='" + uploaderUsername + '\'' +
                ", approvedByAdminUsername='" + approvedByAdminUsername + '\'' +
                ", uploadDate=" + uploadDate +
                ", categories=" + categories +
                ", rating=" + rating +
                '}';
    }
}
