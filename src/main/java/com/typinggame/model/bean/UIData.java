package com.typinggame.model.bean;

import com.typinggame.service.typinggame.CaretState;

import java.io.Serializable;

/**
 * Created by Bence on 2016.10.19..
 */
// welcome to the most boring class ever
public class UIData implements Serializable {

    private float wpm;
    private float accuracy;
    private CaretState caretState;
    private int caretPosition;
    private boolean isEnded;

    public UIData() {
        wpm = 0.0f;
        accuracy = 0.0f;
        caretState = CaretState.DEFAULT;
        isEnded = false;
        caretPosition = 0;
    }

    public float getWpm() {
        return wpm;
    }

    public void setWpm(float wpm) {
        this.wpm = wpm;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public CaretState getCaretState() {
        return caretState;
    }

    public void setCaretState(CaretState caretState) {
        this.caretState = caretState;
    }

    public int getCaretPosition() {
        return caretPosition;
    }

    public void setCaretPosition(int caretPosition) {
        this.caretPosition = caretPosition;
    }

    public boolean isEnded() {
        return isEnded;
    }

    public void setEnded(boolean ended) {
        isEnded = ended;
    }

    @Override
    public String toString() {
        return "UIData{" +
                "wpm=" + wpm +
                ", accuracy=" + accuracy +
                ", caretState=" + caretState +
                '}';
    }
}
