package com.typinggame.model.bean;

public enum QuoteStatus {
    Accepted,
    Rejected,
    Pending,
    Invalid;

    public static final int REJECTED_CODE = 0;
    public static final int ACCEPTED_CODE = 1;
    public static final int PENDING_CODE = 2;

    public static QuoteStatus fromStatusCode(int code) {
        switch (code) {
            case ACCEPTED_CODE:
                return Accepted;
            case REJECTED_CODE:
                return Rejected;
            case PENDING_CODE:
                return Pending;
            default:
                return Invalid;
        }
    }
}
