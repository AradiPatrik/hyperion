package com.typinggame.model.pojo.exception;

public class RatingOutOfBoundsException extends Exception {
    public RatingOutOfBoundsException() {
    }

    public RatingOutOfBoundsException(String message) {
        super(message);
    }

    public RatingOutOfBoundsException(String message, Throwable cause) {
        super(message, cause);
    }

    public RatingOutOfBoundsException(Throwable cause) {
        super(cause);
    }

    public RatingOutOfBoundsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
