package com.typinggame.model.pojo;

import com.typinggame.model.pojo.exception.RatingOutOfBoundsException;

public class Rating {
    public Double value;
    private static final double MIN = 0.0;
    private static final double MAX = 5.0;
    private static final String UNDERFLOW = "Rating underflow";
    private static final String OVERFLOW = "Rating overflow";

    public Rating(Double value) throws RatingOutOfBoundsException {
        if (value < MIN) {
            throw new RatingOutOfBoundsException(UNDERFLOW);
        } else if (value > MAX) {
            throw new RatingOutOfBoundsException(OVERFLOW);
        } else {
            this.value = value;
        }
    }

}
