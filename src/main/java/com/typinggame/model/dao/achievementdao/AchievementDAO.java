package com.typinggame.model.dao.achievementdao;

import com.typinggame.model.bean.Achievement;
import com.typinggame.model.bean.User;

import java.util.List;

public interface AchievementDAO {
    List<Achievement> getAllAchievement();

    List<Achievement> getAchievementsOfUser(User user);
}
