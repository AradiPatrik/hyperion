package com.typinggame.model.dao.achievementdao;

import com.google.inject.Inject;
import com.typinggame.model.ConnectionFactory;
import com.typinggame.model.bean.Achievement;
import com.typinggame.model.bean.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OracleDBAchievementDAOImpl implements AchievementDAO {
    private static final String SELECT_ACHIEVEMENTS = "SELECT * FROM Achivement";
    private static final String SELECT_ACHIEVEMENTS_OF_USER = "SELECT name, value FROM achivement "
            + "INNER JOIN achieves ON achivement.name = achieves.achivement_name "
            + "WHERE  achieves.USER_USERNAME = ?";
    private ConnectionFactory connectionFactory;

    @Inject
    public OracleDBAchievementDAOImpl(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public List<Achievement> getAllAchievement() {
        List<Achievement> achievements = null;
        try (Connection connection = connectionFactory.create()) {
            achievements = getAllAchievementFromDB(connection);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return achievements;
    }

    private List<Achievement> getAllAchievementFromDB(Connection connection) throws SQLException {
        List<Achievement> achievements = new ArrayList<>();
        Statement ps = connection.createStatement();
        ResultSet rs = ps.executeQuery(SELECT_ACHIEVEMENTS);
        while (rs.next()) {
            achievements.add(createAchievementFromResult(rs));
        }
        return achievements;
    }

    private Achievement createAchievementFromResult(ResultSet rs) throws SQLException {
        Achievement achievement = new Achievement();
        achievement.setName(rs.getString(1));
        achievement.setPointValue(rs.getInt(2));
        return achievement;
    }

    @Override
    public List<Achievement> getAchievementsOfUser(User user) {
        List<Achievement> achievements = null;
        try (Connection connection = connectionFactory.create()) {
            achievements = getAchievementsOfUserFromDB(connection, user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return achievements;
    }

    private List<Achievement> getAchievementsOfUserFromDB(Connection connection, User user) throws SQLException {
        List<Achievement> achievements = new ArrayList<>();
        PreparedStatement ps = connection.prepareStatement(SELECT_ACHIEVEMENTS_OF_USER);
        ps.setString(1, user.getUsername());
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            achievements.add(createAchievementFromResult(rs));
        }
        return achievements;
    }
}

