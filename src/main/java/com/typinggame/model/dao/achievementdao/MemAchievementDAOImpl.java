package com.typinggame.model.dao.achievementdao;

import com.typinggame.model.bean.Achievement;
import com.typinggame.model.bean.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MemAchievementDAOImpl implements AchievementDAO {

    private List<Achievement> achievements;
    private List<AchievesPojo> achievesPojos;

    public MemAchievementDAOImpl() {
        achievements = provideDataForAchievements();
        achievesPojos = provideDataForAchievesPojos();
    }

    private List<AchievesPojo> provideDataForAchievesPojos() {
        List<AchievesPojo> achievesPojos = new ArrayList<>();
        achievesPojos.add(new AchievesPojo("Peti", "Play 25 different quotes"));
        achievesPojos.add(new AchievesPojo("Peti", "Get 30 wpm on 100 different quotes"));
        achievesPojos.add(new AchievesPojo("Peti", "Upload 5 quotes"));
        achievesPojos.add(new AchievesPojo("Tomi", "Get 30 wpm on 100 different quotes"));
        achievesPojos.add(new AchievesPojo("Tomi", "Get 50 wpm on 100 different quotes"));
        achievesPojos.add(new AchievesPojo("test", "Upload 5 quotes"));
        return achievesPojos;
    }

    private List<Achievement> provideDataForAchievements() {
        List<Achievement> achievements = new ArrayList<>();
        achievements.add(new Achievement("Play 25 different quotes", 5));
        achievements.add(new Achievement("Play 50 different quotes", 10));
        achievements.add(new Achievement("Play 100 different quotes", 15));
        achievements.add(new Achievement("Get 30 wpm on 100 different quotess", 10));
        achievements.add(new Achievement("Get 50 wpm on 100 different quotes", 15));
        achievements.add(new Achievement("Get 70 wpm on 100 different quotes", 20));
        achievements.add(new Achievement("Upload 5 quotes", 5));
        achievements.add(new Achievement("Upload 10 quotes", 10));
        achievements.add(new Achievement("Upload 20 quotes", 20));
        achievements.add(new Achievement("Upload 50 quotes", 25));
        return achievements;
    }

    @Override
    public List<Achievement> getAllAchievement() {
        return achievements;
    }

    @Override
    public List<Achievement> getAchievementsOfUser(User user) {
        List<AchievesPojo> achievesPojosOfUser = achievesPojos.stream()
                .filter(e -> e.username.equals(user.getUsername()))
                .collect(Collectors.toList());
        return achievements.stream()
                .filter(e -> achievesPojosOfUser.stream().anyMatch(p -> e.getName().equals(p.AchievementName)))
                .collect(Collectors.toList());
    }

    private class AchievesPojo {
        public String username;
        public String AchievementName;

        public AchievesPojo(String username, String achievementName) {
            this.username = username;
            AchievementName = achievementName;
        }
    }

}
