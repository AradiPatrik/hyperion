package com.typinggame.model.dao.userdao;

import com.typinggame.model.bean.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MemUserDAOImpl implements UserDAO {
    private List<User> users;

    public MemUserDAOImpl() {
        users = new ArrayList<>();
        provideData();
    }

    private void provideData() {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        try {
            users.add(new User(
                    "Csaba",
                    "csaba123",
                    df.parse("16-04-2017"),
                    null
            ));
            users.add(new User(
                    "Hannah",
                    "hannah123",
                    df.parse("11-05-2017"),
                    null
            ));
            users.add(new User(
                    "Lea",
                    "lea123",
                    df.parse("14-06-2017"),
                    null
            ));
            users.add(new User(
                    "Christian",
                    "chris123",
                    df.parse("25-07-2017"),
                    null
            ));
            users.add(new User(
                    "Ditta",
                    "ditta123",
                    df.parse("11-08-2017"),
                    null
            ));
            users.add(new User(
                    "Patrik",
                    "abc123",
                    df.parse("16-09-2017"),
                    null
            ));
            users.add(new User(
                    "test",
                    "test",
                    df.parse("20-10-1994"),
                    null
            ));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<User> getUser(String username) {
        return users.stream().filter(e -> e.getUsername().equals(username)).findFirst();
    }

    @Override
    public void addUser(User user) {
        users.add(user);
    }

    @Override
    public void deleteUser(User user) {
        users.stream()
                .filter(e -> e.getUsername().equals(user.getUsername()) && e.getPassword().equals(user.getPassword()))
                .findFirst()
                .ifPresent(e -> users.remove(e));
    }
}
