package com.typinggame.model.dao.userdao;

import com.typinggame.model.bean.User;

import java.util.Optional;

public interface UserDAO {
    Optional<User> getUser(String username);

    void addUser(User user);

    void deleteUser(User user);
}
