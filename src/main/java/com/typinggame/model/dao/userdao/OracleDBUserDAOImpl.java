package com.typinggame.model.dao.userdao;

import com.google.inject.Inject;
import com.typinggame.model.ConnectionFactory;
import com.typinggame.model.bean.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Optional;

public class OracleDBUserDAOImpl implements UserDAO {
    private static final String selectUserWithUsername = "SELECT * FROM USERS WHERE USERNAME = ?";
    private static final String addUser = "insert into USERS(USERNAME, PASSWORD) values (?,?)";
    private static final String deleteUser = "DELETE FROM USERS WHERE USERNAME = ? AND PASSWORD = ?";
    DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
    private ConnectionFactory connectionFactory;

    @Inject
    public OracleDBUserDAOImpl(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public Optional<User> getUser(String username) {
        Optional<User> result = Optional.empty();
        try (Connection connection = connectionFactory.create()) {
            result = getUserFromDB(connection, username);
        } catch (SQLException e) {
            System.out.println("Error code: " + e.getErrorCode() + " State: " + e.getSQLState());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    private Optional<User> getUserFromDB(Connection connection, String username) throws Exception {
        Optional<User> result = Optional.empty();
        PreparedStatement ps = connection.prepareStatement(selectUserWithUsername);
        ps.setString(1, username);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            result = Optional.of(createUserFromResult(rs));
        }
        return result;
    }

    private User createUserFromResult(ResultSet rs) throws Exception {
        User result = new User();
        result.setUsername(rs.getString(1));
        result.setPassword(rs.getString(2));
        result.setRegisterDate(dateFormat.parse(rs.getString(3)));
        return result;
    }

    @Override
    public void addUser(User user) {
        try (Connection connection = connectionFactory.create()) {
            addUserToDB(connection, user);
        } catch (SQLException e) {
            System.out.println("Error code: " + e.getErrorCode() + " State: " + e.getSQLState());
            e.printStackTrace();
        }
    }

    private void addUserToDB(Connection connection, User user) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(addUser);
        ps.setString(1, user.getUsername());
        ps.setString(2, user.getPassword());
        ps.execute();
    }

    @Override
    public void deleteUser(User user) {
        try (Connection connection = connectionFactory.create()) {
            deleteUserFromDB(connection, user);
        } catch (SQLException e) {
            System.out.println("Error code: " + e.getErrorCode() + " State: " + e.getSQLState());
        }
    }

    private void deleteUserFromDB(Connection connection, User user) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(deleteUser);
        ps.setString(1, user.getUsername());
        ps.setString(2, user.getPassword());
        ps.execute();
    }
}
