package com.typinggame.model.dao.admindao;

public interface AdminDAO {
    boolean authenticateAdmin(String username, String password);
}
