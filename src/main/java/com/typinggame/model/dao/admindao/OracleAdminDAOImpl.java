package com.typinggame.model.dao.admindao;

import com.google.inject.Inject;
import com.typinggame.model.ConnectionFactory;
import com.typinggame.model.bean.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OracleAdminDAOImpl implements AdminDAO {
    private static final String GET_ADMIN_WITH_PASSWORD = "SELECT * FROM ADMINS WHERE USERNAME = ? AND PASSWORD = ?";
    private ConnectionFactory connectionFactory;

    @Inject
    public OracleAdminDAOImpl(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public boolean authenticateAdmin(String username, String password) {
        try(Connection connection = connectionFactory.create()) {
            PreparedStatement statement = connection.prepareStatement(GET_ADMIN_WITH_PASSWORD);
            statement.setString(1, username);
            statement.setString(2, password);
            return statement.executeQuery().next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false; // never reached
    }


}
