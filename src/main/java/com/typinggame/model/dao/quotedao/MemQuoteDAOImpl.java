package com.typinggame.model.dao.quotedao;

import com.typinggame.model.bean.Category;
import com.typinggame.model.bean.Quote;
import com.typinggame.model.bean.QuoteStatus;

import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class MemQuoteDAOImpl implements QuoteDAO {

    // TODO: 3/5/2018 test

    private final List<Quote> quotes;

    public MemQuoteDAOImpl() {
        quotes = new ArrayList<>();
        provideData();
    }

    private void provideData() {
        quotes.add(new Quote(1, "Ede", "Szeretem a nyarat", "A nyar szep", "patrik", Date.from(Instant.now()), new ArrayList<>(Arrays.asList(new Category("drama"))), QuoteStatus.Accepted));
        quotes.add(new Quote(2, "Peti", "Szerelmem hangjai", "Hát az amolyan szép gondolat. Gondolat miként egy pinty vinnyog. Hol hagytam a tollamat. Én nem tudom miért írok", "patrik", Date.from(Instant.parse("2007-12-03T10:15:30.00Z")), new ArrayList<>(Arrays.asList(new Category("drama"))), QuoteStatus.Accepted));
        quotes.add(new Quote(3, "Emil", "Nem en mondtam", "Nem én mondtam, háromszor, háromszor, háromszor. Nem én mondtam háromszor, háromszor, háromszor.", "patrik", Date.from(Instant.parse("2017-12-03T10:15:30.00Z")), new ArrayList<>(Arrays.asList(new Category("drama"))), QuoteStatus.Accepted));
        quotes.add(new Quote(4, "Patrik", "Utalok Dummy datat irni", "Miként az erős tölgyet kidőlti a szél, én úgy utálok Dummy adatot írni, végbél.", "patrik", Date.from(Instant.parse("2013-12-03T10:15:30.00Z")), new ArrayList<>(Arrays.asList(new Category("drama"))), QuoteStatus.Accepted));
        quotes.add(new Quote(5, "Ede", "Szeretem a telet is", "A tél szép a nyár csúnya. A kis cica ha meg nem rúgja. Háromszor háromszor háromszor.", "patrik", Date.from(Instant.parse("2011-12-03T10:15:30.00Z")), new ArrayList<>(Arrays.asList(new Category("drama"))), QuoteStatus.Accepted));
        quotes.add(new Quote(6, "Ede", "De a oszt nem szeretem", "Az ősz is csúnya, bár szeretem a telet, hát tőlem ez is kitellet. Szeretem még a szomszéd Pistát is. Vennék tőle angol leckét meg franciát is.", "patrik", Date.from(Instant.parse("2000-11-03T10:15:30.00Z")), new ArrayList<>(Arrays.asList(new Category("drama"))), QuoteStatus.Accepted));
        quotes.add(new Quote(7, "Emil", "Bolognai", "Engem var otthon egy nagy adag bolognai, szoval azt hiszem ma sem fogyok le", "patrik", Date.from(Instant.parse("2000-11-03T10:15:30.00Z")), new ArrayList<>(Arrays.asList(new Category("drama"))), QuoteStatus.Accepted));
        quotes.add(new Quote(8, "Patrik", "Bolognai re", "Én nem tudom miért ennél ilyen kaját. Megől a glutén, igen, nahát. Bár én sem beszélhetek, mert nekem a szenvedélyem a carbonara. De csak néz előre és menjél arra.", "patrik", Date.from(Instant.parse("2003-11-03T10:15:30.00Z")), new ArrayList<>(Arrays.asList(new Category("drama"))), QuoteStatus.Accepted));
        quotes.add(new Quote(9, "Peti", "Nem szeretek senkit", "Mindenki hidek, az élet sem szép. Mit ér ilyenkor a vitéz akarat. Zúg a táj, hideg az ég. Meg rendül ilyenkor a vitéz akarat.", "patrik", Date.from(Instant.parse("2006-11-03T10:15:30.00Z")), new ArrayList<>(Arrays.asList(new Category("drama"))), QuoteStatus.Accepted));
        quotes.add(new Quote(10, "Lehel", "Sziasztok sracok", "Sziasztok srácok, melegen várok. Melegen várok, rátok. Miért baj ez kérdem én, hisz oly rövid az élet játszani elég", "patrik", Date.from(Instant.parse("2001-11-03T10:15:30.00Z")), new ArrayList<>(Arrays.asList(new Category("drama"))), QuoteStatus.Pending));
        quotes.add(new Quote(11, "Lehel", "Sziasztok sracok", "Sziasztok kacsa melegíti a légteret. Melegen várok, rátok. Miért baj ez kérdem én, hisz oly rövid az élet játszani elég", "patrik", Date.from(Instant.parse("2001-11-03T10:15:30.00Z")), new ArrayList<>(Arrays.asList(new Category("Poem"))), QuoteStatus.Accepted));
        quotes.add(new Quote(12, "Lehel", "Sziasztok sracok", "Sziasztok srácok, meleg78954621rövid az élet játszani elég", "patrik", Date.from(Instant.parse("2001-11-03T10:15:30.00Z")), new ArrayList<>(Arrays.asList(new Category("drama"))), QuoteStatus.Pending));
    }

    @Override
    public Quote getRandomQuote() {
        List<Quote> ResultQuotes = filterForQuotesForAcceptedStatus(quotes);
        int randomNum = ThreadLocalRandom.current().nextInt(0, ResultQuotes.size());
        return ResultQuotes.get(randomNum);
    }

    @Override
    public Quote getQuoteWithId(int id) throws IOException {
        Optional<Quote> result = quotes.stream()
                .filter(quote -> quote.getId() == id)
                .findFirst();
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new IOException();
        }
    }

    @Override
    public List<Quote> getAllQuotes() {
        return filterForQuotesForAcceptedStatus(quotes);
    }

    @Override
    public void addQuote(Quote quote) {
        quote.setQuoteStatus(QuoteStatus.Pending);
        this.quotes.add(quote);
    }

    @Override
    public void deleteQuote(int id) {
        quotes.removeIf(quote -> quote.getId() == id);
    }

    @Override
    public List<Quote> getPendingQuotes() {
        return quotes.stream().filter(e -> e.getQuoteStatus().equals(QuoteStatus.Pending)).collect(Collectors.toList());
    }

    @Override
    public void approveQuote(int id, String adminUsername) throws IOException {
        Quote quote = getQuoteWithId(id);
        quote.setQuoteStatus(QuoteStatus.Accepted);
        quote.setApprovedByAdminUsername(adminUsername);
    }

    @Override
    public List<Quote> getQuotesOfCategory(Category category) {
        return filterForQuotesForAcceptedStatus(quotes).stream()
                .filter(e -> e.getCategories().stream().anyMatch(p -> p.getName().equals(category.getName())))
                .collect(Collectors.toList());
    }

    private List<Quote> filterForQuotesForAcceptedStatus(List<Quote> quotes) {
        return (new ArrayList<>(quotes)).stream().filter(e -> e.getQuoteStatus().equals(QuoteStatus.Accepted)).collect(Collectors.toList());
    }
}
