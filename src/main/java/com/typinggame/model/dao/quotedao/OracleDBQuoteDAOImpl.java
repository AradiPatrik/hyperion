package com.typinggame.model.dao.quotedao;

import com.google.inject.Inject;
import com.typinggame.model.ConnectionFactory;
import com.typinggame.model.bean.Category;
import com.typinggame.model.bean.Quote;
import com.typinggame.model.bean.QuoteStatus;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OracleDBQuoteDAOImpl implements QuoteDAO {

    private static final String selectRandomQuote = "SELECT * FROM ( SELECT * FROM QUOTE WHERE QUOTE_STATUS = " + QuoteStatus.ACCEPTED_CODE + "ORDER BY dbms_random.value ) WHERE rownum = 1";
    private static final String selectAllQuotes = "SELECT * FROM QUOTE WHERE QUOTE_STATUS = " + QuoteStatus.ACCEPTED_CODE;
    private static final String selectQuotesOfCategory = "SELECT ID, AUTHOR, TITLE, TEXT, UPLOAD_DATE, ADMIN_USERNAME, USER_USERNAME, QUOTE_STATUS FROM QUOTE INNER JOIN BELONGSTOCATEGORY ON BELONGSTOCATEGORY.QUOTE_ID = ID WHERE BELONGSTOCATEGORY.CATEGORY_NAME = ? AND QUOTE_STATUS = " + QuoteStatus.ACCEPTED_CODE;
    private static final String selectQuoteWithID = "SELECT * FROM QUOTE WHERE ID = ?";
    private static final String addQuote = "INSERT INTO quote(AUTHOR, TITLE, TEXT, USER_USERNAME, QUOTE_STATUS) values (?,?,?,?,?)";
    private static final String deleteQuote = "DELETE FROM QUOTE WHERE ID = ?";
    private ConnectionFactory connectionFactory;

    @Inject
    public OracleDBQuoteDAOImpl(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public Quote getRandomQuote() {
        Quote resultQuote = null;
        try (Connection connection = connectionFactory.create()) {
            CallableStatement callableStatement = connection.prepareCall(selectRandomQuote, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = callableStatement.executeQuery();
            if (rs.next()) {
                resultQuote = createQuoteFromResult(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resultQuote;
    }

    @Override
    public Quote getQuoteWithId(int id) throws IOException {
        Quote resultQuote = null;
        try (Connection connection = connectionFactory.create()) {
            resultQuote = getQuoteWithIdFromDB(connection, id);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resultQuote;
    }

    private Quote getQuoteWithIdFromDB(Connection connection, int id) throws IOException, SQLException {
        Quote resultQuote;
        PreparedStatement ps = connection.prepareStatement(selectQuoteWithID);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            resultQuote = createQuoteFromResult(rs);
        } else {
            throw new IOException("There is no Quote with given ID!");
        }
        return resultQuote;
    }

    private Quote createQuoteFromResult(ResultSet rs) throws SQLException {
        Quote resultQuote = new Quote();
        resultQuote.setId(rs.getInt(1));
        resultQuote.setAuthor(rs.getString(2));
        resultQuote.setTitle(rs.getString(3));
        resultQuote.setText(rs.getString(4));
        resultQuote.setUploadDate(rs.getDate(5));
        resultQuote.setApprovedByAdminUsername(rs.getString(6));
        resultQuote.setUploaderUsername(rs.getString(7));
        resultQuote.setQuoteStatus(QuoteStatus.fromStatusCode(rs.getInt(8)));
        return resultQuote;
    }

    @Override
    public List<Quote> getAllQuotes() {
        List<Quote> resultQuotes = new ArrayList<>();
        try (Connection connection = connectionFactory.create()) {
            resultQuotes = getAllQuotesFromDB(connection);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resultQuotes;
    }

    private List<Quote> getAllQuotesFromDB(Connection connection) throws SQLException {
        List<Quote> resultQuotes = new ArrayList<>();
        Statement ps = connection.createStatement();
        ResultSet rs = ps.executeQuery(selectAllQuotes);
        while (rs.next()) {
            Quote resultQuote = createQuoteFromResult(rs);
            resultQuotes.add(resultQuote);
        }
        return resultQuotes;
    }


    @Override
    public void addQuote(Quote quote) {
        try (Connection connection = connectionFactory.create()) {
            addQuoteToDB(connection, quote);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void addQuoteToDB(Connection connection, Quote quote) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(addQuote);
        ps.setString(1, quote.getAuthor());
        ps.setString(2, quote.getTitle());
        ps.setString(3, quote.getText());
        ps.setString(4, quote.getUploaderUsername());
        ps.setInt(5, QuoteStatus.PENDING_CODE);
        ps.execute();
    }

    @Override
    public List<Quote> getQuotesOfCategory(Category category) {
        List<Quote> resultQuotes = new ArrayList<>();
        try (Connection connection = connectionFactory.create()) {
            PreparedStatement ps = connection.prepareStatement(selectQuotesOfCategory);
            ps.setString(1, category.getName());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Quote resultQuote = createQuoteFromResult(rs);
                resultQuotes.add(resultQuote);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resultQuotes;
    }

    @Override
    public void deleteQuote(int id) {
        try (Connection connection = connectionFactory.create()) {
            deleteQuoteFromDB(connection, id);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<Quote> getPendingQuotes() {
        return null;
    }

    @Override
    public void approveQuote(int id, String adminUsername) {

    }

    private void deleteQuoteFromDB(Connection connection, int id) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(deleteQuote);
        ps.setInt(1, id);
        ps.execute();
    }
}
