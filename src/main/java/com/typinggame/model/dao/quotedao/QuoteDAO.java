package com.typinggame.model.dao.quotedao;

import com.typinggame.model.bean.Category;
import com.typinggame.model.bean.Quote;

import java.io.IOException;
import java.util.List;

public interface QuoteDAO {
    Quote getRandomQuote();

    Quote getQuoteWithId(int id) throws IOException;

    List<Quote> getAllQuotes();

    List<Quote> getQuotesOfCategory(Category category);

    void addQuote(Quote quote);

    void deleteQuote(int id);

    List<Quote> getPendingQuotes();

    void approveQuote(int id, String adminUsername) throws IOException;
}
