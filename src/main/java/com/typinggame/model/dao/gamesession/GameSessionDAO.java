package com.typinggame.model.dao.gamesession;

import com.typinggame.model.bean.GameSession;
import com.typinggame.model.bean.User;

import java.util.List;

public interface GameSessionDAO {
    boolean addGameSession(GameSession gameSession);

    List<GameSession> getGameSessionOfUsername(String username);
}
