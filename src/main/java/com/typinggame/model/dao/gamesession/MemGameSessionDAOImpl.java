package com.typinggame.model.dao.gamesession;

import com.typinggame.model.bean.GameSession;
import com.typinggame.model.bean.User;

import java.util.ArrayList;
import java.util.List;

public class MemGameSessionDAOImpl implements GameSessionDAO {

    private List<GameSession> gameSessions;

    public MemGameSessionDAOImpl() {
        gameSessions = provideData();
    }

    private List<GameSession> provideData() {
        List<GameSession> gameSessions = new ArrayList<>();
        return gameSessions;
    }


    @Override
    public boolean addGameSession(GameSession gameSession) {
        gameSessions.add(gameSession);
        return true;
    }

    @Override
    public List<GameSession> getGameSessionOfUsername(String username) {
        return gameSessions;
    }
}
