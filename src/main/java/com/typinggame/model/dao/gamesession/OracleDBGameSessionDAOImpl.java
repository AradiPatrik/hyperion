package com.typinggame.model.dao.gamesession;

import com.google.inject.Inject;
import com.typinggame.model.ConnectionFactory;
import com.typinggame.model.bean.GameSession;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OracleDBGameSessionDAOImpl implements GameSessionDAO {
    private static final String SELECT_GAME_SESSION_OF_USER = "SELECT * FROM PRACTICE WHERE USER_USERNAME = ?";
    private static final String INSERT_GAME_SESSION_OF_USER_AND_QUOTE = "INSERT INTO PRACTICE (USER_USERNAME, QUOTE_ID, WPM, ACCURACY) VALUES(?, ?, ?, ?)";
    private static final int CHEATER_ERROR_CODE = 20101;
    private ConnectionFactory connectionFactory;

    @Inject
    public OracleDBGameSessionDAOImpl(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public boolean addGameSession(GameSession gameSession) {
        try (Connection connection = connectionFactory.create()) {
            return addGameSessionToDB(connection, gameSession);
        } catch (SQLException e) {
            if (e.getErrorCode() == CHEATER_ERROR_CODE) {
                System.out.println("caught a cheater");
            } else {
                e.printStackTrace();
            }
        }
        return false;
    }

    private boolean addGameSessionToDB(Connection connection, GameSession gameSession) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(INSERT_GAME_SESSION_OF_USER_AND_QUOTE);
        int valueIndex = 1;
        ps.setString(valueIndex++, gameSession.getUsername());
        ps.setInt(valueIndex++, gameSession.getQuoteID());
        ps.setDouble(valueIndex++, gameSession.getWpm());
        ps.setDouble(valueIndex++, gameSession.getAccuracy());
        return ps.execute();
    }

    @Override
    public List<GameSession> getGameSessionOfUsername(String username) {
        try (Connection connection = connectionFactory.create()) {
            return getGameSessionOfUsernameFromDB(connection, username);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<GameSession> getGameSessionOfUsernameFromDB(Connection connection, String username) throws SQLException {
        List<GameSession> gameSessions = new ArrayList<>();
        PreparedStatement ps = connection.prepareStatement(SELECT_GAME_SESSION_OF_USER);
        ps.setString(1, username);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Date date = rs.getDate("PRACTICE_TIME");
            int quoteID = rs.getInt("QUOTE_ID");
            double wpm = rs.getDouble("WPM");
            double accuracy = rs.getDouble("ACCURACY");
            gameSessions.add(new GameSession(wpm, accuracy, date, quoteID, username));
        }
        return gameSessions;
    }
}
