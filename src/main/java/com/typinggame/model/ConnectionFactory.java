package com.typinggame.model;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import oracle.jdbc.pool.OracleDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionFactory {
    private String connectionName;
    private String connectionPassword;
    private OracleDataSource oracleDataSource;

    @Inject
    public ConnectionFactory(
            @Named("dbURL") String dbURL,
            @Named("connectionName") String connectionName,
            @Named("connectionPassword") String connectionPassword
    ) throws ClassNotFoundException, SQLException {
        this.connectionName = connectionName;
        this.connectionPassword = connectionPassword;
        oracleDataSource = new OracleDataSource();
        Class.forName("oracle.jdbc.OracleDriver");
        oracleDataSource.setURL(dbURL);
    }

    public Connection create() throws SQLException {
        return oracleDataSource.getConnection(connectionName, connectionPassword);
    }
}
