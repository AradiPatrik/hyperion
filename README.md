# README #

### Project Hyperion ###

* This application will be a typing game
* This project focused originally on creating an application with GUI, following the mvc pattern
* The original goal was to get familiar with gui applications, and get experience in working as a team
* Now the project shifted, and is about learning how to hook up a GUI application to a database, so we can manage user profiles and statistics

### How do I get set up? ###

* You need an ide that can load a gradle project, and load build.gradle.

### Contribution guidelines ###

* Open branch for the issue which you are working on. This can be achieved by using Boards on
Bitbucket
* Always create pull requests, so that others can review your code
* The commits should be flagged with: 
> * [f] - (feature) if you worked on a task that added feature
> * [m] - (modification) something that changed currently existing behaviour
> * [r] - (refactor) small changes in code structure

* The commits should be flagged with an [Affected Area: ...] to help others, to pinpoint where, the changes took place
> ###Commit message example
> [m][Affected Area: Logging] Changed logging to use Apache loggers instead of System.out.println.
* The response to a pull request should be
> * [good to go] If you are ready to merge changes
> * [clarify] If you need further clarification about implementation details
> * [issue] If you think, that the change can't be merged in it's current change

###Code Style
1. If in doubt about code formatting consult [the oracle standard](http://www.oracle.com/technetwork/java/codeconventions-150003.pdf).
2. Always use functional programming constructs (lambdas instead of anonymous inner classes, streams instead of for loops), wherever possible. [It makes your code more readable,
and avoids ceremony](https://www.youtube.com/watch?v=1OpAgZvYXLQ). Exception to the rule, is when you just want to iterate over elements in a collection; in this case, you should use range based for loops.
```java
public static void bad(List<Dog> dogs) {
    list.foreach(e -> e.bark());
}

public static void good(List<Dog> dogs) {
    for (Dog dog : dogs) {
        dog.bark();
    }
}
```

